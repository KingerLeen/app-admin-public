# coding: utf-8
from __future__ import unicode_literals

from django.contrib.sites.models import Site
from django.db import models
from django.utils.datetime_safe import date, datetime

from DjangoUeditor.models import UEditorField

#***************************************************************************************

class Advertisement(models.Model):
    '''
        广告表
    '''
    class Meta:
        verbose_name = u'广告'
        verbose_name_plural = verbose_name

    ad_id = models.AutoField(u'广告号', primary_key=True)
    ad_url = models.URLField(u'链接', blank=True, default="")
    ad_photo = models.ImageField(upload_to='contents/', verbose_name=u'图片', blank=True, default="")
    is_play = models.NullBooleanField(u'播放')
    create_time = models.DateTimeField(u'创建时间', auto_now_add=True)
    update_time = models.DateTimeField(u'更新时间', auto_now=True)

    def __unicode__(self):
        return self.ad_url

class User(models.Model):
    '''
        用户表
    '''
    class Meta:
        verbose_name = u'用户'
        verbose_name_plural = verbose_name

    user_id = models.AutoField(u'用户号', primary_key=True)
    phone_number = models.CharField(u'电话号码', max_length=50, default="")
    name = models.CharField(u'姓名', max_length=50, default="")
    password = models.CharField(u'用户密码', max_length=200, default="")
    icon = models.ImageField(u'用户头像', upload_to='contents/', blank=True, default="")
    rank = models.CharField(u'用户等级',max_length=20, blank=True, default="")
    is_manager = models.NullBooleanField(u'经理')
    invite_code = models.CharField(u'邀请码', max_length=50, default="")
    invited_code = models.CharField(u'被邀请码', max_length=50, blank=True, default="")
    point = models.IntegerField(u'积分', null=True, blank=True, default=0)
    collect = models.CommaSeparatedIntegerField(u'收藏产品', max_length=500, blank=True, default="")
    create_time = models.DateTimeField(u'创建时间', auto_now_add=True)
    update_time = models.DateTimeField(u'更新时间', auto_now=True)

    def __unicode__(self):
        return self.phone_number

class Bank(models.Model):
    '''
        银行表
    '''
    class Meta:
        verbose_name = u'银行'
        verbose_name_plural = verbose_name

    bank_id = models.AutoField(u'银行号', primary_key=True)
    bank_name = models.CharField(u'银行名', max_length=20, blank=True, default="")
    bank_icon = models.ImageField(u'银行图片', upload_to='contents/', blank=True, default="")
    create_time = models.DateTimeField(u'创建时间', auto_now_add=True)
    update_time = models.DateTimeField(u'更新时间', auto_now=True)
    
    def __unicode__(self):
        return self.bank_name

class Product(models.Model):
    '''
        产品表
    '''
    class Meta:
        verbose_name = u'产品'
        verbose_name_plural = verbose_name

    product_id = models.AutoField(u'产品号', primary_key=True)
    product_manager = models.ForeignKey(User, verbose_name=u'产品经理', related_name='+')
    product_name = models.CharField(u'产品名', max_length=20, blank=True, default="")
    product_icon = models.ImageField(u'产品图片', upload_to='contents/', blank=True, default="")
    product_bank = models.ForeignKey(Bank, verbose_name=u'银行', related_name='+')
    tag = models.CharField(u'标签', max_length=500, blank=True, default="")
    ioan_type = models.CharField(u'贷款类型', max_length=20, blank=True, default="",
    choices = ((u"车贷",u"车贷"),(u"房贷",u"房贷"),(u"信贷",u"信贷")))
    application = models.TextField(u'申请条件', max_length=500, blank=True, default="")
    limit_scope = models.CharField(u'额度范围', max_length=50, blank=True, default="")
    term_scope = models.CharField(u'期限范围', max_length=50, blank=True, default="")
    monthly_interest_rate_scope = models.CharField(u'月利率范围', max_length=50, blank=True, default="")
    poundage = models.CharField(u'手续费', max_length=50, blank=True, default="")
    service_cost = models.CharField(u'服务费用', max_length=50, blank=True, default="")
    required_information = models.TextField(u'所需资料', max_length=500, blank=True, default="")
    recommend = models.NullBooleanField(u'首页推荐')
    create_time = models.DateTimeField(u'创建时间', auto_now_add=True)
    update_time = models.DateTimeField(u'更新时间', auto_now=True)

    def __unicode__(self):
        return self.product_name

class Order(models.Model):
    '''
        订单表
    '''
    class Meta:
        verbose_name = u'订单'
        verbose_name_plural = verbose_name

    order_id = models.AutoField(u'订单号', primary_key=True)
    order_state = models.CharField(u'订单状态', max_length=20, blank=True, default="")
    product_id = models.ForeignKey(Product, verbose_name=u'产品号', related_name='+')
    user_id = models.ForeignKey(User, verbose_name=u'用户号', related_name='+')
    ioan_amount = models.IntegerField(u'贷款金额', null=True, blank=True, default=0)
    ioan_term = models.CharField(u'贷款期限', max_length=20, blank=True, default="")
    expect_earnings = models.IntegerField(u'预计收益', null=True, blank=True, default=0)
    create_time = models.DateTimeField(u'创建时间', auto_now_add=True)
    update_time = models.DateTimeField(u'更新时间', auto_now=True)

    def __unicode__(self):
        return self.ioan_term

class Advise(models.Model):
    '''
        帮助与建议表
    '''
    class Meta:
        verbose_name = u'帮助与建议'
        verbose_name_plural = verbose_name

    advise_id = models.AutoField(u'建议号', primary_key=True)
    user_id = models.ForeignKey(User, verbose_name=u'用户号', related_name='+')
    feedback_content = models.CharField(u'反馈内容', max_length=500, blank=True, default="")
    create_time = models.DateTimeField(u'创建时间', auto_now_add=True)
    update_time = models.DateTimeField(u'更新时间', auto_now=True)

    def __unicode__(self):
        return self.feedback_content


class Rule(models.Model):
    '''
        规则表
    '''
    class Meta:
        verbose_name = u'规则'
        verbose_name_plural = verbose_name
    
    rule_id = models.AutoField(u'规则号',primary_key=True)
    evaluation_results = models.CharField(u'评测结果', max_length=500, blank=True, default="")
    sex = models.CharField(u'性别', max_length=5, blank=True, default="")
    limit = models.IntegerField(u'额度', null=True, blank=True, default=0)
    age = models.IntegerField(u'年龄', null=True, blank=True, default=0)
    region = models.CharField(u'地区', max_length=100, blank=True, default="")
    household_register = models.CharField(u'户籍', max_length=100, blank=True, default="")
    marital_status = models.CharField(u'婚姻状况', max_length=5, blank=True, default="")
    monthly_salary = models.IntegerField(u'月薪', null=True, blank=True, default=0)
    fixed_assets = models.CharField(u'固定资产', max_length=100, blank=True, default="")
    accumulation_fund = models.NullBooleanField(u'公积金信息')
    name = models.CharField(u'客户姓名', max_length=20, blank=True, default="")
    mail = models.EmailField(u'邮箱', blank=True, default="")
    phone_number = models.CharField(u'电话号码', max_length=20, blank=True, default=0)
    career_type = models.CharField(u'职业类别', max_length=20, blank=True, default="")
    customer_qualification = models.CharField(u'客户资质', max_length=20, blank=True, default="")
    id_card = models.CharField(u'身份证号码', max_length=30, blank=True, default="")
    id_card_scanner = models.ImageField(u'身份证扫描件', upload_to='contents/', blank=True, default="")
    id_card_copy = models.ImageField(u'身份证复印件', upload_to='contents/', blank=True, default="")
    handheld_card_photo = models.ImageField(u'客户手持身份证照片', upload_to='contents/', blank=True, default="")
    is_letter_credit = models.NullBooleanField(u'是否征信')
    enclosure = models.ImageField(u'征信附件', upload_to='contents/', blank=True, default="")
    remarks = models.CharField(u'客户信息备注', max_length=200, blank=True, default="")
    work_phone = models.CharField(u'单位座机', max_length=20, blank=True, default="")
    work_nature = models.CharField(u'单位性质', max_length=20, blank=True, default="")
    work_address = models.CharField(u'单位地址', max_length=200, blank=True, default="")
    work_name = models.CharField(u'工作单位名称', max_length=200, blank=True, default="")
    contact_name = models.CharField(u'联系人姓名', max_length=20, blank=True, default="")
    contact_number = models.CharField(u'联系人电话号码', max_length=20, blank=True, default="")
    contact_relationship = models.CharField(u'联系人关系', max_length=20, blank=True, default="")
    social_security = models.CharField(u'社保信息', max_length=20, blank=True, default="")
    create_time = models.DateTimeField(u'创建时间', auto_now_add=True)
    update_time = models.DateTimeField(u'更新时间', auto_now=True)

    def __unicode__(self):
        return self.evaluation_results



class Test(models.Model):
    '''
        评测表
    '''
    class Meta:
        verbose_name = u'评测'
        verbose_name_plural = verbose_name

    isHave = ((True,u"有"),(False,u"无"))
    isRight = ((True,u"是"),(False,u"否"))
    isCan = ((True,u"可以"),(False,u"不可以"))

    test_id = models.AutoField(u'评测号', primary_key=True)
    user_id = models.ForeignKey(User, verbose_name=u'用户号', related_name='+')
    test_tag = models.CharField(u'评测类型', max_length=50, blank=True, default="",
    choices = ((u"智能优选",u"智能优选"),(u"手动评测",u"手动评测")))


    is_property = models.NullBooleanField(u'是否房产', choices = isHave)
    property_status = models.CharField(u'房产状态',max_length=50, blank=True, default="",
    choices = ((u"全款房",u"全款房"),(u"按揭房",u"按揭房"),(u"抵押房",u"抵押房")))
    full_house = models.CharField(u'全款房',max_length=50, blank=True, default="",
    choices = ((u"全款房房子面积",u"全款房房子面积"),(u"房产数量",u"房产数量"),(u"全款房房产类别",u"全款房房产类别"),
    (u"全款房房产年限",u"全款房房产年限"),(u"相关证件状态",u"相关证件状态"),(u"全款房房产估值",u"全款房房产估值"),(u"全款房房产区域",u"全款房房产区域")))
    mortgage_house = models.CharField(u'按揭房',max_length=50, blank=True, default="",
    choices = ((u"按揭房房子面积",u"按揭房房子面积"),(u"贷款金额",u"贷款金额"),(u"按揭房房产类别",u"按揭房房产类别"),
    (u"按揭房房产年限",u"按揭房房产年限"),(u"房产按揭月供时间",u"房产按揭月供时间"),(u"按揭房房产估值",u"按揭房房产估值"),(u"按揭房房产区域",u"按揭房房产区域"),
    (u"按揭房每月还款额",u"按揭房每月还款额"),(u"按揭月供方式",u"按揭月供方式")))
    pledge_house = models.CharField(u'抵押房', max_length=50, blank=True, default="",
    choices = ((u"抵押房房子面积",u"抵押房房子面积"),(u"抵押机构",u"抵押机构"),(u"抵押房房产类别",u"抵押房房产类别"),
    (u"抵押房房产年限",u"抵押房房产年限"),(u"抵押房房产估值",u"抵押房房产估值"),(u"抵押房房产区域",u"抵押房房产区域"),(u"房产抵押还款方式",u"房产抵押还款方式"),
    (u"抵押房每月还款额",u"抵押房每月还款额"),(u"已抵押时间",u"已抵押时间"),(u"房产抵押金额",u"房产抵押金额")))
    property_number = models.CharField(u'房产数量', max_length=50, blank=True, default="",
    choices = ((u"1套",u"1套"),(u"2套",u"2套"),(u"3套及以上",u"3套及以上")))
    relevant_document_status = models.CharField(u'相关证件状态', max_length=200, blank=True, default="",
    choices = ((u"已有房产证",u"已有房产证"),(u"已有土地使用证",u"已有土地使用证"),(u"已有不动产权证",u"已有不动产权证"),(u"无",u"无")))
    full_house_area = models.CharField(u'全款房房子面积', max_length=200, blank=True, default="",
    choices = ((u"50平方以下",u"50平方以下"),(u"50-90平方",u"50-90平方"),(u"90平方以上",u"90平方以上")))
    mortgage_house_area = models.CharField(u'按揭房房子面积', max_length=200, blank=True, default="",
    choices = ((u"50平方以下",u"50平方以下"),(u"50-90平方",u"50-90平方"),(u"90平方以上",u"90平方以上")))
    pleage_house_area = models.CharField(u'抵押车房子面积', max_length=200, blank=True, default="",
    choices = ((u"50平方以下",u"50平方以下"),(u"50-90平方",u"50-90平方"),(u"90平方以上",u"90平方以上")))
    full_property_category = models.CharField(u'全款房房产类别', max_length=50, blank=True, default="",
    choices = ((u"住宅",u"住宅"),(u"自建",u"自建"),(u"公寓",u"公寓"),(u"铺面",u"铺面"),(u"写字楼",u"写字楼")))
    mortgage_property_category = models.CharField(u'按揭房房产类别', max_length=50, blank=True, default="",
    choices = ((u"住宅",u"住宅"),(u"自建",u"自建"),(u"公寓",u"公寓"),(u"铺面",u"铺面"),(u"写字楼",u"写字楼")))
    pledge_property_category = models.CharField(u'抵押房房产类别', max_length=50, blank=True, default="",
    choices = ((u"住宅",u"住宅"),(u"自建",u"自建"),(u"公寓",u"公寓"),(u"铺面",u"铺面"),(u"写字楼",u"写字楼")))
    full_real_estate_area = models.CharField(u'全款房房产区域', max_length=50, blank=True, default="",
    choices = ((u"省",u"省"),(u"市",u"市"),(u"区",u"区")))
    mortgage_real_estate_area = models.CharField(u'按揭房房产区域', max_length=50, blank=True, default="",
    choices = ((u"省",u"省"),(u"市",u"市"),(u"区",u"区")))
    pledge_real_estate_area = models.CharField(u'抵押房房产区域', max_length=50, blank=True, default="",
    choices = ((u"省",u"省"),(u"市",u"市"),(u"区",u"区")))
    full_property_year = models.CharField(u'全款房房产年限', max_length=200, blank=True, default="",
    choices = ((u"2年以下",u"2年以下"),(u"2-5年",u"2-5年"),(u"5-20年",u"5-20年"),(u"20年以上",u"20年以上")))
    mortgage_property_year = models.CharField(u'按揭房房产年限', max_length=200, blank=True, default="",
    choices = ((u"2年以下",u"2年以下"),(u"2-5年",u"2-5年"),(u"5-20年",u"5-20年"),(u"20年以上",u"20年以上")))
    pledge_property_year = models.CharField(u'抵押房房产年限', max_length=200, blank=True, default="",
    choices = ((u"2年以下",u"2年以下"),(u"2-5年",u"2-5年"),(u"5-20年",u"5-20年"),(u"20年以上",u"20年以上")))
    full_property_valuation = models.CharField(u'全款房房产估值', max_length=200, blank=True, default="",
    choices = ((u"50万以下",u"50万以下"),(u"50-100万",u"50-100万"),(u"100-200万",u"100-200万"),(u"200万以上",u"200万以上")))
    mortgage_property_valuation = models.CharField(u'按揭房房产估值', max_length=200, blank=True, default="",
    choices = ((u"50万以下",u"50万以下"),(u"50-100万",u"50-100万"),(u"100-200万",u"100-200万"),(u"200万以上",u"200万以上")))
    pledge_property_valuation = models.CharField(u'抵押房房产估值', max_length=200, blank=True, default="",
    choices = ((u"50万以下",u"50万以下"),(u"50-100万",u"50-100万"),(u"100-200万",u"100-200万"),(u"200万以上",u"200万以上")))
    mortgage_monthly_time_house = models.CharField(u'房产按揭月供时间', max_length=200, blank=True, default="",
    choices = ((u"一个月以下",u"一个月以下"),(u"1-3个月",u"1-3个月"),(u"3-6个月",u"3-6个月"),(u"6-12个月",u"6-12个月"),
    (u"12-24个月",u"12-24个月"),(u"24个月以上",u"24个月以上")))
    loan_amount = models.CharField(u'贷款金额', max_length=200, blank=True, default="",
    choices = ((u"20万以下",u"20万以下"),(u"20-30万",u"20-30万"),(u"30-50万",u"30-50万"),(u"50万以上",u"50万以上")))
    mortgage_monthly_payment_method = models.CharField(u'按揭月供方式', max_length=200, blank=True, default="",
    choices = ((u"等额本息",u"等额本息"),(u"先息后本",u"先息后本")))
    mortgage_monthly_repayment_amount = models.CharField(u'按揭房每月还款额', max_length=200, blank=True, default="",
    choices = ((u"1000以下",u"1000以下"),(u"1000-2000",u"1000-2000"),(u"2000-4000",u"2000-4000"),(u"4000及以上",u"4000及以上")))
    pledge_monthly_repayment_amount = models.CharField(u'抵押房每月还款额', max_length=200, blank=True, default="",
    choices = ((u"1000以下",u"1000以下"),(u"1000-2000",u"1000-2000"),(u"2000-4000",u"2000-4000"),(u"4000及以上",u"4000及以上")))
    pledge_time = models.CharField(u'已抵押时间', max_length=200, blank=True, default="",
    choices = ((u"一个月以下",u"一个月以下"),(u"1-3个月",u"1-3个月"),(u"3-6个月",u"3-6个月"),(u"6-12个月",u"6-12个月"),
    (u"12-24个月",u"12-24个月"),(u"24个月以上",u"24个月以上")))
    pledge_agency_house = models.CharField(u'房产抵押机构', max_length=200, blank=True, default="",
    choices = ((u"银行",u"银行"),(u"非银行",u"非银行")))
    pledge_repayment_method_house = models.CharField(u'房产抵押还款方式', max_length=200, blank=True, default="",
    choices = ((u"等额本息",u"等额本息"),(u"先息后本",u"先息后本")))
    pledge_amount_house = models.CharField(u'房产抵押金额', max_length=200, blank=True, default="",
    choices = ((u"20万以下",u"20万以下"),(u"20-30万",u"20-30万"),(u"30-50万",u"30-50万"),(u"50万以上",u"50万以上")))


    is_car = models.NullBooleanField(u'是否有车', choices = isHave)
    car_status = models.CharField(u'车辆状态', max_length=200, blank=True, default="",
    choices = ((u"全款车",u"全款车"),(u"按揭车",u"按揭车"),(u"抵押车",u"抵押车")))
    full_car = models.CharField(u'全款车', max_length=200, blank=True, default="",
    choices = ((u"全款车车辆行驶里程",u"全款车车辆行驶里程"),(u"全款车车牌归属地是否为本地车牌",u"全款车车牌归属地是否为本地车牌"),
    (u"全款车车辆裸车价",u"全款车车辆裸车价"),(u"全款车车辆使用年限",u"全款车车辆使用年限"),(u"全款车车辆过户时间",u"全款车车辆过户时间"),
    (u"全款车是否刚过户",u"全款车是否刚过户"),(u"全款车车况如何",u"全款车车况如何"),(u"全款车是否在本人名下",u"全款车是否在本人名下")))
    pledge_car = models.CharField(u'抵押车', max_length=200, blank=True, default="",
    choices = ((u"抵押车车辆行驶里程",u"抵押车车辆行驶里程"),(u"抵押车车牌归属地是否为本地车牌",u"抵押车车牌归属地是否为本地车牌"),
    (u"抵押车车辆裸车价",u"抵押车车辆裸车价"),(u"抵押车车辆使用年限",u"抵押车车辆使用年限"),(u"抵押车车辆过户时间",u"抵押车车辆过户时间"),
    (u"抵押车是否刚过户",u"抵押车是否刚过户"),(u"抵押车车况如何",u"抵押车车况如何"),(u"抵押车是否在本人名下",u"抵押车是否在本人名下"),
    (u"车辆抵押金额",u"车辆抵押金额"),(u"车辆抵押月供时间",u"车辆抵押月供时间"),(u"车辆抵押机构",u"车辆抵押机构"),(u"车辆抵押还款方式",u"车辆抵押还款方式")))
    mortgage_car = models.CharField(u'按揭车', max_length=200, blank=True, default="",
    choices = ((u"按揭车车辆行驶里程",u"按揭车车辆行驶里程"),(u"按揭车车牌归属地是否为本地车牌",u"按揭车车牌归属地是否为本地车牌"),
    (u"按揭车车辆裸车价",u"按揭车车辆裸车价"),(u"按揭车车辆使用年限",u"按揭车车辆使用年限"),(u"按揭车车辆过户时间",u"按揭车车辆过户时间"),
    (u"按揭车是否刚过户",u"按揭车是否刚过户"),(u"按揭车车况如何",u"按揭车车况如何"),(u"按揭车是否在本人名下",u"按揭车是否在本人名下"),
    (u"车辆贷款金额",u"车辆贷款金额"),(u"车辆按揭月供时间",u"车辆按揭月供时间"),(u"按揭机构",u"按揭机构"),(u"按揭还款方式",u"按揭还款方式")))
    is_full_license_plate_attribution = models.NullBooleanField(u'全款车车牌归属地是否为本地车牌', choices = isRight)
    is_mortgage_license_plate_attribution = models.NullBooleanField(u'按揭车车牌归属地是否为本地车牌', choices = isRight)
    is_pledge_license_plate_attribution = models.NullBooleanField(u'抵押车车牌归属地是否为本地车牌', choices = isRight)
    full_vehicle_mileage = models.CharField(u'全款车车辆行驶里程', max_length=200, blank=True, default="",
    choices = ((u"1万公里以下",u"1万公里以下"),(u"1-2万公里",u"1-2万公里"),(u"2-5万公里",u"2-5万公里"),
    (u"5-10万公里",u"5-10万公里"),(u"10万公里以上",u"10万公里以上")))
    mortgage_vehicle_mileage = models.CharField(u'按揭车车辆行驶里程', max_length=200, blank=True, default="",
    choices = ((u"1万公里以下",u"1万公里以下"),(u"1-2万公里",u"1-2万公里"),(u"2-5万公里",u"2-5万公里"),
    (u"5-10万公里",u"5-10万公里"),(u"10万公里以上",u"10万公里以上")))
    pledge_vehicle_mileage = models.CharField(u'抵押车车辆行驶里程', max_length=200, blank=True, default="",
    choices = ((u"1万公里以下",u"1万公里以下"),(u"1-2万公里",u"1-2万公里"),(u"2-5万公里",u"2-5万公里"),
    (u"5-10万公里",u"5-10万公里"),(u"10万公里以上",u"10万公里以上")))
    full_vehicle_bare_car_price = models.CharField(u'全款车车辆裸车价', max_length=200, blank=True, default="",
    choices = ((u"10万以下",u"10万以下"),(u"10-20万",u"10-20万"),(u"20-30万",u"20-30万"),
    (u"30万及以上",u"30万及以上")))
    mortgage_vehicle_bare_car_price = models.CharField(u'按揭车车辆裸车价', max_length=200, blank=True, default="",
    choices = ((u"10万以下",u"10万以下"),(u"10-20万",u"10-20万"),(u"20-30万",u"20-30万"),
    (u"30万及以上",u"30万及以上")))
    pledge_vehicle_bare_car_price = models.CharField(u'抵押车车辆裸车价', max_length=200, blank=True, default="",
    choices = ((u"10万以下",u"10万以下"),(u"10-20万",u"10-20万"),(u"20-30万",u"20-30万"),
    (u"30万及以上",u"30万及以上")))
    full_vehicle_life = models.CharField(u'全款车车辆使用年限', max_length=200, blank=True, default="",
    choices = ((u"1年以内",u"1年以内"),(u"1-3年",u"1-3年"),(u"3-8年",u"3-8年"),
    (u"8年及以上",u"8年及以上")))
    mortgage_vehicle_life = models.CharField(u'按揭车车辆使用年限', max_length=200, blank=True, default="",
    choices = ((u"1年以内",u"1年以内"),(u"1-3年",u"1-3年"),(u"3-8年",u"3-8年"),
    (u"8年及以上",u"8年及以上")))
    pledge_vehicle_life = models.CharField(u'抵押车车辆使用年限', max_length=200, blank=True, default="",
    choices = ((u"1年以内",u"1年以内"),(u"1-3年",u"1-3年"),(u"3-8年",u"3-8年"),
    (u"8年及以上",u"8年及以上")))
    full_vehicle_transfer_time = models.CharField(u'全款车车辆过户时间', max_length=200, blank=True, default="",
    choices = ((u"1个月以下",u"1个月以下"),(u"1-3个月",u"1-3个月"),(u"3-6个月",u"3-6个月"),
    (u"6个月及以上",u"6个月及以上")))
    mortgage_vehicle_transfer_time = models.CharField(u'按揭车车辆过户时间', max_length=200, blank=True, default="",
    choices = ((u"1个月以下",u"1个月以下"),(u"1-3个月",u"1-3个月"),(u"3-6个月",u"3-6个月"),
    (u"6个月及以上",u"6个月及以上")))
    pledge_vehicle_transfer_time = models.CharField(u'抵押车车辆过户时间', max_length=200, blank=True, default="",
    choices = ((u"1个月以下",u"1个月以下"),(u"1-3个月",u"1-3个月"),(u"3-6个月",u"3-6个月"),
    (u"6个月及以上",u"6个月及以上")))
    is_full_transfer = models.NullBooleanField(u'全款车是否刚过户', choices = isRight)
    is_mortgage_transfer = models.NullBooleanField(u'按揭车是否刚过户', choices = isRight)
    is_pledge_transfer = models.NullBooleanField(u'抵押车是否刚过户', choices = isRight)
    full_car_condition = models.CharField(u'全款车车况如何', max_length=200, blank=True, default="",
    choices = ((u"良好",u"良好"),(u"一般",u"一般"),(u"差",u"差")))
    mortgage_car_condition = models.CharField(u'按揭车车况如何', max_length=200, blank=True, default="",
    choices = ((u"良好",u"良好"),(u"一般",u"一般"),(u"差",u"差")))
    pledge_car_condition = models.CharField(u'抵押车车况如何', max_length=200, blank=True, default="",
    choices = ((u"良好",u"良好"),(u"一般",u"一般"),(u"差",u"差")))
    is_full_name = models.NullBooleanField(u'全款车是否在本人名下', choices = isRight)
    is_mortgage_name = models.NullBooleanField(u'按揭车是否在本人名下', choices = isRight)
    is_pledge_name = models.NullBooleanField(u'抵押车是否在本人名下', choices = isRight)
    pledge_amount_car = models.CharField(u'车辆抵押金额', max_length=200, blank=True, default="",
    choices = ((u"5万以下",u"5万以下"),(u"5-10万",u"5-10万"),(u"10-20万",u"10-20万"),(u"20万以上",u"20万以上")))
    pledge_agency_car = models.CharField(u'车辆抵押机构', max_length=200, blank=True, default="",
    choices = ((u"银行",u"银行"),(u"非银行",u"非银行")))
    pledge_monthly_time_car = models.CharField(u'车辆抵押月供时间', max_length=200, blank=True, default="",
    choices = ((u"一个月以下",u"一个月以下"),(u"1-3个月",u"1-3个月"),(u"3-6个月",u"3-6个月"),(u"6-12个月",u"6-12个月"),
    (u"12个月以上",u"12个月以上")))
    vehicle_loan_amount = models.CharField(u'车辆贷款金额', max_length=200, blank=True, default="",
    choices = ((u"5万以下",u"5万以下"),(u"5-10万",u"5-10万"),(u"10-20万",u"10-20万"),(u"20万以上",u"20万以上")))
    mortgage_monthly_time_car = models.CharField(u'车辆按揭月供时间', max_length=200, blank=True, default="",
    choices = ((u"一个月以下",u"一个月以下"),(u"1-3个月",u"1-3个月"),(u"3-6个月",u"3-6个月"),(u"6-12个月",u"6-12个月"),
    (u"12个月以上",u"12个月以上")))
    mortgage_agency = models.CharField(u'按揭机构', max_length=200, blank=True, default="",
    choices = ((u"银行",u"银行"),(u"非银行",u"非银行")))
    mortgage_repayment_method = models.CharField(u'按揭还款方式', max_length=200, blank=True, default="",
    choices = ((u"等额本息",u"等额本息"),(u"先息后本",u"先息后本")))
    pledge_repayment_method_car = models.CharField(u'车辆抵押还款方式', max_length=200, blank=True, default="",
    choices = ((u"等额本息",u"等额本息"),(u"先息后本",u"先息后本")))


    policy_annual_payment_amount = models.CharField(u'保单年缴费金额', max_length=200, blank=True, default="",
    choices = ((u"2400以下",u"2400以下"),(u"2400-5000",u"2400-5000"),(u"5000-1万",u"5000-1万"),(u"1-5万",u"1-5万"),
    (u"5万以上",u"5万以上")))
    insurance_company = models.CharField(u'保险公司', max_length=200, blank=True, default="",
    choices = ((u"平安人寿",u"平安人寿"),(u"中国人寿",u"中国人寿"),(u"太平人寿",u"太平人寿"),(u"华夏人寿",u"华夏人寿"),
    (u"其他",u"其他")))
    payment_method = models.CharField(u'缴费方式', max_length=200, blank=True, default="",
    choices = ((u"月缴费",u"月缴费"),(u"季缴费",u"季缴费"),(u"年缴费",u"年缴费"),(u"趸缴费",u"趸缴费")))
    is_insured = models.NullBooleanField(u'是否为投保人', choices = isRight)
    is_replace_insured = models.NullBooleanField(u'是否更换投保人', choices = isRight)
    is_commercial_insurance_break = models.NullBooleanField(u'商业保险是否断缴', choices = isRight)
    warranty = models.CharField(u'份保单', max_length=200, blank=True, default="",
    choices = ((u"一份",u"一份"),(u"二份",u"二份"),(u"三份及以上",u"三份及以上")))
    is_buy_commercial_insurance = models.NullBooleanField(u'是否购买商业保险', choices = isHave)
    borrower_age = models.CharField(u'借款人年龄(岁)', max_length=200, blank=True, default="",
    choices = ((u"18-20",u"18-20"),(u"20-25",u"20-25"),(u"25-55",u"25-55"),(u"55-60",u"55-60"),
    (u"60-65",u"60-65"),(u"65以上",u"65以上")))
    is_domicile = models.NullBooleanField(u'户籍所在地是否为本地', choices = isRight)
    local_residence_time = models.CharField(u'本地居住时间', max_length=200, blank=True, default="",
    choices = ((u"3个月以下",u"3个月以下"),(u"3-6个月",u"3-6个月"),(u"6-12个月",u"6-12个月"),(u"12个月及以上",u"12个月及以上")))
    is_divorce_kid = models.CharField(u'离异是否有孩子', max_length=200, blank=True, default="",
    choices = ((u"没有",u"没有"),(u"一个",u"一个"),(u"两个",u"两个"),(u"两个以上",u"两个以上")))
    is_married_kid = models.CharField(u'已婚是否有孩子', max_length=200, blank=True, default="",
    choices = ((u"没有",u"没有"),(u"一个",u"一个"),(u"两个",u"两个"),(u"两个以上",u"两个以上")))
    is_unmarried_kid = models.CharField(u'未婚是否有孩子', max_length=200, blank=True, default="",
    choices = ((u"没有",u"没有"),(u"一个",u"一个"),(u"两个",u"两个"),(u"两个以上",u"两个以上")))
    marital_status = models.CharField(u'婚姻状况', max_length=200, blank=True, default="",
    choices = ((u"离异",u"离异"),(u"未婚",u"未婚"),(u"已婚",u"已婚")))
    total_loan_amount = models.CharField(u'贷款总金额', max_length=200, blank=True, default="",
    choices = ((u"5万以下",u"5万以下"),(u"5-10万",u"5-10万"),(u"10-20万",u"10-20万"),(u"20万以上",u"20万以上")))
    monthly_debt = models.CharField(u'负债月供多少', max_length=200, blank=True, default="",
    choices = ((u"5000以下",u"5000以下"),(u"5000-10000",u"5000-10000"),(u"10000-20000",u"10000-20000"),(u"20000及以上",u"20000及以上")))
    total_debt_amount = models.CharField(u'负债总金额', max_length=200, blank=True, default="",
    choices = ((u"5万以下",u"5万以下"),(u"5-10万",u"5-10万"),(u"10-20万",u"10-20万"),(u"20万以上",u"20万以上")))
    is_mortgage_liability = models.NullBooleanField(u'抵押负债', choices = isHave)
    total_credit_cards = models.CharField(u'信用卡总数', max_length=200, blank=True, default="",
    choices = ((u"1张",u"1张"),(u"2-4张",u"2-4张"),(u"5张及以上",u"5张及以上")))
    credit_card_total = models.CharField(u'信用卡总额度', max_length=200, blank=True, default="",
    choices = ((u"1万以下",u"1万以下"),(u"1-2万",u"1-2万"),(u"2-5万",u"2-5万"),(u"5-10万",u"5-10万"),
    (u"10万以上",u"10万以上")))
    amount_used = models.CharField(u'已使用金额', max_length=200, blank=True, default="",
    choices = ((u"1万以下",u"1万以下"),(u"1-2万",u"1-2万"),(u"2-5万",u"2-5万"),(u"5-10万",u"5-10万"),
    (u"10万以上",u"10万以上")))
    is_credit_card_liability = models.NullBooleanField(u'信用卡负债', choices = isHave)
    total_credit_liability = models.CharField(u'信贷总负债', max_length=200, blank=True, default="",
    choices = ((u"1万以下",u"1万以下"),(u"1-2万",u"1-2万"),(u"2-5万",u"2-5万"),(u"5-10万",u"5-10万"),
    (u"10万以上",u"10万以上")))
    small_loan_total_loan_amount = models.CharField(u'小额贷总放款额度', max_length=200, blank=True, default="",
    choices = ((u"1万以下",u"1万以下"),(u"1-2万",u"1-2万"),(u"2-5万",u"2-5万"),(u"5-10万",u"5-10万"),
    (u"10万以上",u"10万以上")))
    microfinance_institutions_number = models.CharField(u'小额贷放款机构数量', max_length=200, blank=True, default="",
    choices = ((u"一家",u"一家"),(u"二家",u"二家"),(u"三家及以上",u"三家及以上")))
    credit_report_number = models.CharField(u'小额贷款征信体现几家', max_length=200, blank=True, default="",
    choices = ((u"一家",u"一家"),(u"二家",u"二家"),(u"三家及以上",u"三家及以上")))
    is_microfinance_credit = models.NullBooleanField(u'小额贷款是否上征信', choices = isRight)
    is_microfinance_company_issues_loans = models.NullBooleanField(u'小额贷公司是否发放贷款', choices = isRight)
    consumer_finance_company_total_loan_amount = models.CharField(u'消费金融公司总放款额度', max_length=200, blank=True, default="",
    choices = ((u"1万以下",u"1万以下"),(u"1-2万",u"1-2万"),(u"2-5万",u"2-5万"),(u"5-10万",u"5-10万"),
    (u"10万以上",u"10万以上")))
    consumer_finance_lending_institution_number = models.CharField(u'消费金融放款机构数量', max_length=200, blank=True, default="",
    choices = ((u"一家",u"一家"),(u"二家",u"二家"),(u"三家及以上",u"三家及以上")))
    consumer_finance_credit_report_number = models.CharField(u'消费金融机构征信体现几家', max_length=200, blank=True, default="",
    choices = ((u"一家",u"一家"),(u"二家",u"二家"),(u"三家及以上",u"三家及以上")))
    is_consumer_finance = models.NullBooleanField(u'消费金融机构是否上征信', choices = isRight)
    is_consumer_finance_company_issue_loans = models.NullBooleanField(u'消费金融公司是否发放贷款', choices = isRight)
    is_credit_liability = models.NullBooleanField(u'信贷负债', choices = isHave)
    debt_amount = models.CharField(u'负债金额', max_length=200, blank=True, default="",
    choices = ((u"1万以下",u"1万以下"),(u"1-2万",u"1-2万"),(u"2-5万",u"2-5万"),(u"5-10万",u"5-10万"),
    (u"10万以上",u"10万以上")))
    is_other_liability = models.NullBooleanField(u'其它负债', choices = isHave)
    is_loan = models.NullBooleanField(u'是否有贷款', choices = isHave)
    is_freeze = models.NullBooleanField(u'贷款、信用卡是否有冻结、止付、呆账、关注、次级、可疑、损失', choices = isRight)
    credit_a_month_number = models.CharField(u'一个月内征信查询次数', max_length=200, blank=True, default="",
    choices = ((u"0",u"0"),(u"1",u"1"),(u"2",u"2"),(u"3",u"3"),(u"4及以上",u"4及以上")))
    credit_two_month_number = models.CharField(u'二个月内征信查询次数', max_length=200, blank=True, default="",
    choices = ((u"0",u"0"),(u"1",u"1"),(u"2",u"2"),(u"3",u"3"),(u"4",u"4"),
    (u"5",u"5"),(u"6",u"6"),(u"7次及以上",u"7次及以上")))
    credit_three_month_number = models.CharField(u'三个月内征信查询次数', max_length=200, blank=True, default="",
    choices = ((u"0",u"0"),(u"1",u"1"),(u"2",u"2"),(u"3",u"3"),(u"4",u"4"),
    (u"5",u"5"),(u"6",u"6"),(u"7次及以上",u"7次及以上")))
    credit_six_month_number = models.CharField(u'六个月内征信查询次数', max_length=200, blank=True, default="",
    choices = ((u"0",u"0"),(u"1",u"1"),(u"2",u"2"),(u"3",u"3"),(u"4",u"4"),
    (u"5",u"5"),(u"6",u"6"),(u"7次及以上",u"7次及以上")))
    is_overdue_now = models.NullBooleanField(u'是否有当前逾期', choices = isHave)
    overdue_amount_now = models.CharField(u'当前逾期金额', max_length=200, blank=True, default="",
    choices = ((u"1千以下",u"1千以下"),(u"1-3千",u"1-3千"),(u"3-5千",u"3-5千"),(u"5千-1万",u"5千-1万"),
    (u"1万以上",u"1万以上")))
    is_overdue_half = models.NullBooleanField(u'半年内有无逾期', choices = isHave)
    overdue_amount_half = models.CharField(u'半年内逾期金额', max_length=200, blank=True, default="",
    choices = ((u"1千以下",u"1千以下"),(u"1-3千",u"1-3千"),(u"3-5千",u"3-5千"),(u"5千-1万",u"5千-1万"),
    (u"1万以上",u"1万以上"))) 
    overdue_time_half = models.CharField(u'半年内逾期时间', max_length=200, blank=True, default="",
    choices = ((u"30-60天",u"30-60天"),(u"60-90天",u"60-90天"),(u"90天以上",u"90天以上"),(u"180天以上",u"180天以上")))
    is_overdue_one = models.NullBooleanField(u'1年内有无逾期', choices = isHave)
    overdue_amount_one = models.CharField(u'一年内逾期金额', max_length=200, blank=True, default="",
    choices = ((u"1千以下",u"1千以下"),(u"1-3千",u"1-3千"),(u"3-5千",u"3-5千"),(u"5千-1万",u"5千-1万"),
    (u"1万以上",u"1万以上")))
    overdue_time_one = models.CharField(u'1年内逾期时间', max_length=200, blank=True, default="",
    choices = ((u"30-60天",u"30-60天"),(u"60-90天",u"60-90天"),(u"90天以上",u"90天以上")))
    is_overdue_two = models.NullBooleanField(u'2年内有无逾期', choices = isHave)
    overdue_amount_two = models.CharField(u'2年内逾期金额', max_length=200, blank=True, default="",
    choices = ((u"1千以下",u"1千以下"),(u"1-3千",u"1-3千"),(u"3-5千",u"3-5千"),(u"5千-1万",u"5千-1万"),
    (u"1万以上",u"1万以上")))
    overdue_time_two = models.CharField(u'2年内逾期时间', max_length=200, blank=True, default="",
    choices = ((u"30-60天",u"30-60天"),(u"60-90天",u"60-90天"),(u"90天以上",u"90天以上")))


    is_business_icense = models.NullBooleanField(u'是否有营业执照', choices = isHave) 
    is_business_icense_me = models.NullBooleanField(u'营业执照和本人是否一致', choices = isHave) 
    is_outside = models.NullBooleanField(u'是否可以外访', choices = isCan) 
    is_work_place = models.NullBooleanField(u'是否有办公场地', choices = isHave) 
    business_license_registration_time = models.CharField(u'营业执照注册时间', max_length=200, blank=True, default="",
    choices = ((u"6个月以下",u"6个月以下"),(u"6-12个月",u"6-12个月"),(u"12-36个月",u"12-36个月"),(u"36个月以上",u"36个月以上")))
    annual_tax_amount = models.CharField(u'年纳税金额', max_length=200, blank=True, default="",
    choices = ((u"5万以下",u"5万以下"),(u"5万-20万",u"5万-20万"),(u"20万-50万",u"20万-50万"),(u"50万以上",u"50万以上")))
    is_normal_taxation = models.NullBooleanField(u'是否正常纳税', choices = isRight)
    company_annual_income = models.CharField(u'公司年收入', max_length=200, blank=True, default="",
    choices = ((u"10万以下",u"10万以下"),(u"20-50万",u"20万-50万"),(u"50-100万",u"50-100万"),(u"100万以上",u"100万以上")))
    occupation = models.CharField(u'职业', max_length=50, blank=True, default="",
    choices = ((u"私企老板",u"私企老板"),(u"无工作",u"无工作"),(u"上班族",u"上班族")))
    monthly_bank_flow = models.CharField(u'每月银行流水大小', max_length=50, blank=True, default="",
    choices = ((u"5000以下",u"5000以下"),(u"5000-10000",u"5000-10000"),(u"10000以上",u"10000以上")))
    is_bank_flow = models.NullBooleanField(u'是否有银行流水', choices = isHave) 
    unit_nature = models.CharField(u'单位性质', max_length=50, blank=True, default="",
    choices = ((u"普通私企",u"普通私企"),(u"国家机关",u"国家机关"),(u"事业单位",u"事业单位"),
    (u"企业单位",u"企业单位"),(u"上市公司",u"上市公司"),(u"世界500强",u"世界500强")))
    salary_payment_method = models.CharField(u'薪资发放方式', max_length=200, blank=True, default="",
    choices = ((u"银行卡",u"银行卡"),(u"现金",u"现金")))
    salary = models.CharField(u'薪资', max_length=200, blank=True, default="",
    choices = ((u"3000以下",u"3000以下"),(u"3000-5000",u"3000-5000"),(u"5000-8000",u"5000-8000"),(u"8000以上",u"8000以上")))
    social_security_base = models.CharField(u'社保基数', max_length=200, blank=True, default="",
    choices = ((u"3000以下",u"3000以下"),(u"3000-4000",u"3000-4000"),(u"4000-6000",u"4000-6000"),
    (u"6000-8000",u"6000-8000"),(u"8000以上",u"8000以上")))
    social_security_continuous_payment_time_social = models.CharField(u'社保连续缴费时间', max_length=200, blank=True, default="",
    choices = ((u"3个月以下",u"3个月以下"),(u"3-6个月",u"3-6个月"),(u"6-12个月",u"6-12个月"),
    (u"12-24个月",u"12-24个月"),(u"24个月以上",u"24个月以上")))
    social_security_payment_time = models.CharField(u'社保断缴时间', max_length=200, blank=True, default="",
    choices = ((u"3个月以下",u"3个月以下"),(u"3-6个月",u"3-6个月"),(u"6-12个月",u"6-12个月"),
    (u"12-24个月",u"12-24个月"),(u"24个月以上",u"24个月以上")))
    is_social_security_paid = models.NullBooleanField(u'社保有无断缴', choices = isHave) 
    social_security_payment_method = models.CharField(u'社保缴纳方式', max_length=50, blank=True, default="",
    choices = ((u"月缴",u"月缴"),(u"季缴",u"季缴"),(u"年缴",u"年缴")))
    social_security_information = models.CharField(u'社保信息', max_length=50, blank=True, default="",
    choices = ((u"有社保",u"有社保"),(u"无社保",u"无社保")))
    provident_fund_base = models.CharField(u'公积金基数', max_length=200, blank=True, default="",
    choices = ((u"3000以下",u"3000以下"),(u"3000-4000",u"3000-4000"),(u"4000-6000",u"4000-6000"),
    (u"6000-8000",u"6000-8000"),(u"8000以上",u"8000以上")))
    provident_fund_continuous_payment_time_provident = models.CharField(u'公积金连续缴费时间', max_length=200, blank=True, default="",
    choices = ((u"3个月以下",u"3个月以下"),(u"3-6个月",u"3-6个月"),(u"6-12个月",u"6-12个月"),
    (u"12-24个月",u"12-24个月"),(u"24个月以上",u"24个月以上")))
    provident_fund_withdrawal_time = models.CharField(u'公积金断缴时间', max_length=200, blank=True, default="",
    choices = ((u"3个月以下",u"3个月以下"),(u"3-6个月",u"3-6个月"),(u"6-12个月",u"6-12个月"),
    (u"12-24个月",u"12-24个月"),(u"24个月以上",u"24个月以上")))
    is_provident_fund_paid = models.NullBooleanField(u'公积金有无断缴', choices = isHave) 
    provident_fund_payment_method = models.CharField(u'公积金缴纳方式', max_length=50, blank=True, default="",
    choices = ((u"月缴",u"月缴"),(u"季缴",u"季缴"),(u"年缴",u"年缴")))
    provident_fund_information = models.CharField(u'公积金信息', max_length=50, blank=True, default="",
    choices = ((u"有公积金",u"有公积金"),(u"无公积金",u"无公积金")))


    sesame_credit_score = models.CharField(u'芝麻信用分值', max_length=50, blank=True, default="",
    choices = ((u"小于500",u"小于500"),(u"500-600",u"500-600"),(u"600-700",u"600-700"),(u"700以上",u"700以上")))
    is_alipay_sesame_credits = models.NullBooleanField(u'是否有支付宝芝麻信用分', choices = isHave) 
    particle_loan_quota = models.CharField(u'微粒贷额度', max_length=200, blank=True, default="",
    choices = ((u"小于1万",u"小于1万"),(u"1万-2万",u"1万-2万"),(u"2万-5万",u"2万-5万"),(u"5万以上",u"5万以上")))
    is_tencent_micro_loan = models.NullBooleanField(u'是否有腾讯微粒贷', choices = isHave) 
    white_line_quota = models.CharField(u'白条额度', max_length=200, blank=True, default="",
    choices = ((u"5000以下",u"5000以下"),(u"5000-10000",u"5000-10000"),(u"10000-50000",u"10000-50000"),(u"50000及以上",u"50000及以上")))
    is_jindong_white_strip = models.NullBooleanField(u'是否有京东白条', choices = isHave) 
    borrowing_quota = models.CharField(u'借呗额度', max_length=200, blank=True, default="",
    choices = ((u"5000以下",u"5000以下"),(u"5000-10000",u"5000-10000"),(u"10000-50000",u"10000-50000"),(u"50000及以上",u"50000及以上")))
    is_ant_borrowing = models.NullBooleanField(u'是否有蚂蚁借呗', choices = isHave) 
    recruitment_quota = models.CharField(u'招联额度', max_length=200, blank=True, default="",
    choices = ((u"5000以下",u"5000以下"),(u"5000-10000",u"5000-10000"),(u"10000-50000",u"10000-50000"),(u"50000及以上",u"50000及以上")))
    is_recruitment_quota = models.NullBooleanField(u'是否有招联额度', choices = isHave) 
    gitzo_quota = models.CharField(u'捷信额度', max_length=200, blank=True, default="",
    choices = ((u"5000以下",u"5000以下"),(u"5000-10000",u"5000-10000"),(u"10000-50000",u"10000-50000"),(u"50000及以上",u"50000及以上")))
    is_gitzo_quota = models.NullBooleanField(u'是否有捷信额度', choices = isHave)

    create_time = models.DateTimeField(u'创建时间', auto_now_add=True)
    update_time = models.DateTimeField(u'更新时间', auto_now=True)

    def __unicode__(self):
        return self.test_tag


class Bookstore(models.Model):
    '''
        秘书库
    '''
    class Meta:
        verbose_name = u'秘书库'
        verbose_name_plural= verbose_name

    book_id = models.AutoField(u'秘书号', primary_key=True)
    book_name = models.CharField(u'秘书名称', max_length=200, blank=True, default="")
    book_number = models.CharField(u'电话号码', max_length=200, blank=True, default="")
    book_tag = models.CharField(u'秘书标签', max_length=50, blank=True, default="")
    book_state = models.CharField(u'接受状态', max_length=50, blank=True, default="")
    create_time = models.DateTimeField(u'创建时间', auto_now_add=True)
    update_time = models.DateTimeField(u'更新时间', auto_now=True)

    def __unicode__(self):
        return self.book_name

class Question(models.Model):
    '''
        常见问题表
    '''
    class Meta:
        verbose_name = u'常见问题'
        verbose_name_plural = verbose_name

    question_id = models.AutoField(u'问题号', primary_key=True)
    user_id = models.ForeignKey(User, verbose_name=u'用户号', related_name='+')
    product_id = models.ForeignKey(Product, verbose_name=u'产品号', related_name='+')
    question_content = models.CharField(u'问题内容', max_length=500, blank=True, default="")
    answer_content = models.CharField(u'回答内容', max_length=500, blank=True, default="")
    is_show = models.NullBooleanField(u'是否显示')
    create_time = models.DateTimeField(u'创建时间', auto_now_add=True)
    update_time = models.DateTimeField(u'更新时间', auto_now=True)

    def __unicode__(self):
        return self.question_content
