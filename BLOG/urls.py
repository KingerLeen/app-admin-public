# coding: utf-8
from django.conf.urls import url, include
from BLOG.views import *
from . import apis

urlpatterns = [
    url(r'^home$', apis.home.home),
    url(r'^register$', apis.register.register),
    url(r'^createToken$', apis.login.createToken),
    url(r'^login$', apis.login.login),
    url(r'^checkToken$', apis.login.checkToken),
    url(r'^question$', apis.question.question),
    url(r'^advise$', apis.advise.advise),
    url(r'^pullBookstore$', apis.bookstore.pullBookstore),
    url(r'^test$', apis.test.test),
    url(r'^pullTest$', apis.test.pullTest),

    url(r'^pullProductRecommend$', apis.product.pullProductRecommend),
    url(r'^pullProductBank$', apis.product.pullProductBank),
    url(r'^pullProduct$', apis.product.pullProduct),




    # url(r'^changeUser$', apis.user.changeUser),
    # url(r'^pullUser$', apis.user.pullUser),
    # url(r'^uploadUserImg$', apis.user.uploadUserImg),

    # url(r'^pullOrder$', apis.order.pullOrder),
    # url(r'^createOrder$', apis.order.createOrder),
    # url(r'^changeOrder$', apis.order.changeOrder),

    # url(r'^createProduct$', apis.product.createProduct),
    # url(r'^changeProduct$', apis.product.changeProduct),
    # url(r'^pullProduct$', apis.product.pullProduct),
    # url(r'^uploadProductIron$', apis.product.uploadProductIron),
    
    # url(r'^pullAD$', apis.advertisement.pullAD),
]