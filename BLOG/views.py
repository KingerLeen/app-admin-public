from django.shortcuts import render, render_to_response
from django.conf import settings

def index(request):
    title = settings.TITLE
    keywords = settings.KEYWORDS
    description = settings.DESCRIPTION

    context = {
        'title': title,
        'keywords': keywords,
        'description': description,
    }
    return render(request, 'index.html', context)

def page_not_found(request):
    return render_to_response('404.html')
