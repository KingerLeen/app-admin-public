#!/usr/bin/env python
# encoding: utf-8

import datetime
import json
import base64
import jwt
import time
import uuid
from django import forms

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from Crypto.Cipher import AES

from ..models import *
from .. import views
from func import *

@csrf_exempt
def pullBookstore(request):
    '''
        拉取bookstore信息
        GET
        传参：{
        }

        返回：{
            "code": *
            "ret":{
                "massage": ***
            }
            "data":{
                //轮播广告
                "bookstore":[
                    {
                        "book_id": ***
                        ...所有信息(不包括创建时间和更新时间)...
                    },
                    ...如果小秘书不止一个，按照上面的格式，一一返回...
                ]
            }
        }
    '''
    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'GET':
        return JsonResponse(response)
    try:
        all_list = Bookstore.objects.values_list('book_id',flat=True)

        def pull(book_id):
            l = Bookstore.objects.get(book_id=book_id)
            data = {}
            data["book_id"]=l.book_id
            data["book_name"]=l.book_name
            data["book_number"]=l.book_number
            data["book_tag"]=l.book_tag
            data["book_state"]=l.book_state
            return data
        
        response = {
            'code': 200,
            'ret': {'message': 'success'},
            "data":{"bookstore":[]}
        }
        for data in all_list:
            response["data"]["bookstore"].append(pull(data))
    except Exception, e:
        response = {'code': 404, 'ret': {'message': str(e)}}
    return JsonResponse(response)