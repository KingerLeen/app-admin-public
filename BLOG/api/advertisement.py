#!/usr/bin/env python
# encoding: utf-8

import datetime
import json
import base64
import jwt
import time
import uuid
from django import forms

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from Crypto.Cipher import AES

from ..models import *
from .. import views

from func import *


@csrf_exempt
def pullAD(request):
    '''
        拉取Advertisement信息
        GET
        传参：{（均为可选参数）
            "filter": "order_id,order_state,*,*" //筛选返回数据,以逗号隔开，如果不填则默认返回所有数据
            "ad_id": *** //若填写则精确返回该订单，若不填，则返回 和该用户相关的(订单经理和用户) 所有订单
        }

        返回：{
            "code": *
            "ret":{
                "massage": ***
            }
            "data":{
                "ad_123" (123为ad_id) :{
                    "ad_id": ***
                    ...所有信息(不包括创建时间和更新时间)...
                    图片返回的是文件地址，例如：media/contents/a.jpeg 
                    加上服务器地址即可直接访问该图片 ip:8000/media/contents/a.jpeg
                },
                //如果订单不止一个，按照上面的格式，一一返回
                    ... ...
            }
        }
    '''
    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'GET':
        return JsonResponse(response)
    try:
        params =  request.GET

        ad_id = params.get('ad_id','')

        all_list = []
        if ad_id == '':
            ad = Advertisement.objects.values_list('ad_id',flat=True)
            all_list += ad
        else:
            all_list.append(Advertisement.objects.get(ad_id=ad_id).ad_id)

        def pullAdvertisementInfo(ad_id, Filter=[]):
            listAdvertisement = Advertisement.objects.get(ad_id=ad_id)
            data = {}
            if not Filter or 'ad_id' in Filter:
                data["ad_id"]=listAdvertisement.ad_id
            if not Filter or 'ad_url' in Filter:
                data["ad_url"]=listAdvertisement.ad_url
            if not Filter or 'ad_photo' in Filter:
                data["ad_photo"]=str(listAdvertisement.ad_photo)
            if not Filter or 'is_play' in Filter:
                data["is_play"]=listAdvertisement.is_play
            return data
        
        response = {
            'code': 200,
            'ret': {'message': 'success'},
            "data":{}
        }

        adFilter = params.get('filter','').replace(' ','')
        if adFilter == '':
            adFilter=[]
        else:
            adFilter = adFilter.split(',')

        for data in all_list:
            response["data"]["ad_"+str(data)] = pullAdvertisementInfo(data, adFilter)
    except Exception, e:
        response = {'code': 404, 'ret': {'message': str(e)}}
    return JsonResponse(response)