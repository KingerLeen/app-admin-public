#!/usr/bin/env python
# encoding: utf-8

import datetime
import json
import base64
import jwt
import time
import uuid
from django import forms

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from Crypto.Cipher import AES

from ..models import *
from .. import views

from func import *




@csrf_exempt
def test(request):
    ''' 
    评测
        POST

        传参：{
            "token":  必填，其余均为选填
            "test_id"  选填，如填写-定位评测追加、修改；没填写该选项-新建评测

            "***"  选填,要插入的参数
                。。。 。。。
        }

        返回：{
            "code": *
            "ret":{
                "massage": *
            }
            "data":{
                "test_id": *
            }
        }
    '''

    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'POST':
        return JsonResponse(response)
    try:
        params = json.loads(request.body)

        token = params.get('token','')
        test_id = params.get('test_id',0)
        
        if not parserToken(token):
            response = {'code': 401, 'ret': {'message': 'token err'}}
            return JsonResponse(response)

        info = token2dic(token)
        user_id = info["user_id"]

        if checkTest(test_id):
            if not Test.objects.get(test_id=test_id).user_id.user_id == user_id:
                response = {'code': 403, 'ret': {'message': 'you have no power'}}
                return JsonResponse(response)
            test = Test.objects.get(test_id=test_id)
        else:
            test = Test.objects.create(
                user_id = User.objects.get(user_id=user_id)
            )

        if params.get('test_tag',''):
            test.test_tag = params.get('test_tag','')
        if params.get('is_property',''):
            test.is_property = params.get('is_property','')
        if params.get('property_status',''):
            test.property_status = params.get('property_status','')
        if params.get('full_house',''):
            test.full_house = params.get('full_house','')
        if params.get('mortgage_house',''):
            test.mortgage_house = params.get('mortgage_house','')
        if params.get('pledge_house',''):
            test.pledge_house = params.get('pledge_house','')
        if params.get('property_number',''):
            test.property_number = params.get('property_number','')
        if params.get('relevant_document_status',''):
            test.relevant_document_status = params.get('relevant_document_status','')
        if params.get('full_house_area',''):
            test.full_house_area = params.get('full_house_area','')
        if params.get('mortgage_house_area',''):
            test.mortgage_house_area = params.get('mortgage_house_area','')
        if params.get('pleage_house_area',''):
            test.pleage_house_area = params.get('pleage_house_area','')
        if params.get('full_property_category',''):
            test.full_property_category = params.get('full_property_category','')
        if params.get('mortgage_property_category',''):
            test.mortgage_property_category = params.get('mortgage_property_category','')
        if params.get('pledge_property_category',''):
            test.pledge_property_category = params.get('pledge_property_category','')
        if params.get('full_real_estate_area',''):
            test.full_real_estate_area = params.get('full_real_estate_area','')
        if params.get('mortgage_real_estate_area',''):
            test.mortgage_real_estate_area = params.get('mortgage_real_estate_area','')
        if params.get('pledge_real_estate_area',''):
            test.pledge_real_estate_area = params.get('pledge_real_estate_area','')
        if params.get('full_property_year',''):
            test.full_property_year = params.get('full_property_year','')
        if params.get('mortgage_property_year',''):
            test.mortgage_property_year = params.get('mortgage_property_year','')
        if params.get('pledge_property_year',''):
            test.pledge_property_year = params.get('pledge_property_year','')
        if params.get('full_property_valuation',''):
            test.full_property_valuation = params.get('full_property_valuation','')
        if params.get('mortgage_property_valuation',''):
            test.mortgage_property_valuation = params.get('mortgage_property_valuation','')
        if params.get('pledge_property_valuation',''):
            test.pledge_property_valuation = params.get('pledge_property_valuation','')
        if params.get('mortgage_monthly_time_house',''):
            test.mortgage_monthly_time_house = params.get('mortgage_monthly_time_house','')
        if params.get('loan_amount',''):
            test.loan_amount = params.get('loan_amount','')
        if params.get('mortgage_monthly_payment_method',''):
            test.mortgage_monthly_payment_method = params.get('mortgage_monthly_payment_method','')
        if params.get('mortgage_monthly_repayment_amount',''):
            test.mortgage_monthly_repayment_amount = params.get('mortgage_monthly_repayment_amount','')
        if params.get('pledge_monthly_repayment_amount',''):
            test.pledge_monthly_repayment_amount = params.get('pledge_monthly_repayment_amount','')
        if params.get('pledge_time',''):
            test.pledge_time = params.get('pledge_time','')
        if params.get('pledge_agency_house',''):
            test.pledge_agency_house = params.get('pledge_agency_house','')
        if params.get('pledge_repayment_method_house',''):
            test.pledge_repayment_method_house = params.get('pledge_repayment_method_house','')
        if params.get('pledge_amount_house',''):
            test.pledge_amount_house = params.get('pledge_amount_house','')
        if params.get('is_car',''):
            test.is_car = params.get('is_car','')
        if params.get('car_status',''):
            test.car_status = params.get('car_status','')
        if params.get('full_car',''):
            test.full_car = params.get('full_car','')
        if params.get('pledge_car',''):
            test.pledge_car = params.get('pledge_car','')
        if params.get('mortgage_car',''):
            test.mortgage_car = params.get('mortgage_car','')
        if params.get('is_full_license_plate_attribution',''):
            test.is_full_license_plate_attribution = params.get('is_full_license_plate_attribution','')
        if params.get('is_mortgage_license_plate_attribution',''):
            test.is_mortgage_license_plate_attribution = params.get('is_mortgage_license_plate_attribution','')
        if params.get('is_pledge_license_plate_attribution',''):
            test.is_pledge_license_plate_attribution = params.get('is_pledge_license_plate_attribution','')
        if params.get('full_vehicle_mileage',''):
            test.full_vehicle_mileage = params.get('full_vehicle_mileage','')
        if params.get('mortgage_vehicle_mileage',''):
            test.mortgage_vehicle_mileage = params.get('mortgage_vehicle_mileage','')
        if params.get('pledge_vehicle_mileage',''):
            test.pledge_vehicle_mileage = params.get('pledge_vehicle_mileage','')
        if params.get('full_vehicle_bare_car_price',''):
            test.full_vehicle_bare_car_price = params.get('full_vehicle_bare_car_price','')
        if params.get('mortgage_vehicle_bare_car_price',''):
            test.mortgage_vehicle_bare_car_price = params.get('mortgage_vehicle_bare_car_price','')
        if params.get('pledge_vehicle_bare_car_price',''):
            test.pledge_vehicle_bare_car_price = params.get('pledge_vehicle_bare_car_price','')
        if params.get('full_vehicle_life',''):
            test.full_vehicle_life = params.get('full_vehicle_life','')
        if params.get('mortgage_vehicle_life',''):
            test.mortgage_vehicle_life = params.get('mortgage_vehicle_life','')
        if params.get('pledge_vehicle_life',''):
            test.pledge_vehicle_life = params.get('pledge_vehicle_life','')
        if params.get('full_vehicle_transfer_time',''):
            test.full_vehicle_transfer_time = params.get('full_vehicle_transfer_time','')
        if params.get('mortgage_vehicle_transfer_time',''):
            test.mortgage_vehicle_transfer_time = params.get('mortgage_vehicle_transfer_time','')
        if params.get('pledge_vehicle_transfer_time',''):
            test.pledge_vehicle_transfer_time = params.get('pledge_vehicle_transfer_time','')
        if params.get('is_full_transfer',''):
            test.is_full_transfer = params.get('is_full_transfer','')
        if params.get('is_mortgage_transfer',''):
            test.is_mortgage_transfer = params.get('is_mortgage_transfer','')
        if params.get('is_pledge_transfer',''):
            test.is_pledge_transfer = params.get('is_pledge_transfer','')
        if params.get('full_car_condition',''):
            test.full_car_condition = params.get('full_car_condition','')
        if params.get('mortgage_car_condition',''):
            test.mortgage_car_condition = params.get('mortgage_car_condition','')
        if params.get('pledge_car_condition',''):
            test.pledge_car_condition = params.get('pledge_car_condition','')
        if params.get('is_full_name',''):
            test.is_full_name = params.get('is_full_name','')
        if params.get('is_mortgage_name',''):
            test.is_mortgage_name = params.get('is_mortgage_name','')
        if params.get('is_pledge_name',''):
            test.is_pledge_name = params.get('is_pledge_name','')
        if params.get('pledge_amount_car',''):
            test.pledge_amount_car = params.get('pledge_amount_car','')
        if params.get('pledge_agency_car',''):
            test.pledge_agency_car = params.get('pledge_agency_car','')
        if params.get('pledge_monthly_time_car',''):
            test.pledge_monthly_time_car = params.get('pledge_monthly_time_car','')
        if params.get('vehicle_loan_amount',''):
            test.vehicle_loan_amount = params.get('vehicle_loan_amount','')
        if params.get('mortgage_monthly_time_car',''):
            test.mortgage_monthly_time_car = params.get('mortgage_monthly_time_car','')
        if params.get('mortgage_agency',''):
            test.mortgage_agency = params.get('mortgage_agency','')
        if params.get('mortgage_repayment_method',''):
            test.mortgage_repayment_method = params.get('mortgage_repayment_method','')
        if params.get('pledge_repayment_method_car',''):
            test.pledge_repayment_method_car = params.get('pledge_repayment_method_car','')
        if params.get('policy_annual_payment_amount',''):
            test.policy_annual_payment_amount = params.get('policy_annual_payment_amount','')
        if params.get('insurance_company',''):
            test.insurance_company = params.get('insurance_company','')
        if params.get('payment_method',''):
            test.payment_method = params.get('payment_method','')
        if params.get('is_insured',''):
            test.is_insured = params.get('is_insured','')
        if params.get('is_replace_insured',''):
            test.is_replace_insured = params.get('is_replace_insured','')
        if params.get('is_commercial_insurance_break',''):
            test.is_commercial_insurance_break = params.get('is_commercial_insurance_break','')
        if params.get('warranty',''):
            test.warranty = params.get('warranty','')
        if params.get('is_buy_commercial_insurance',''):
            test.is_buy_commercial_insurance = params.get('is_buy_commercial_insurance','')
        if params.get('borrower_age',''):
            test.borrower_age = params.get('borrower_age','')
        if params.get('is_domicile',''):
            test.is_domicile = params.get('is_domicile','')
        if params.get('local_residence_time',''):
            test.local_residence_time = params.get('local_residence_time','')
        if params.get('is_divorce_kid',''):
            test.is_divorce_kid = params.get('is_divorce_kid','')
        if params.get('is_married_kid',''):
            test.is_married_kid = params.get('is_married_kid','')
        if params.get('is_unmarried_kid',''):
            test.is_unmarried_kid = params.get('is_unmarried_kid','')
        if params.get('marital_status',''):
            test.marital_status = params.get('marital_status','')
        if params.get('total_loan_amount',''):
            test.total_loan_amount = params.get('total_loan_amount','')
        if params.get('monthly_debt',''):
            test.monthly_debt = params.get('monthly_debt','')
        if params.get('total_debt_amount',''):
            test.total_debt_amount = params.get('total_debt_amount','')
        if params.get('is_mortgage_liability',''):
            test.is_mortgage_liability = params.get('is_mortgage_liability','')
        if params.get('total_credit_cards',''):
            test.total_credit_cards = params.get('total_credit_cards','')
        if params.get('credit_card_total',''):
            test.credit_card_total = params.get('credit_card_total','')
        if params.get('amount_used',''):
            test.amount_used = params.get('amount_used','')
        if params.get('is_credit_card_liability',''):
            test.is_credit_card_liability = params.get('is_credit_card_liability','')
        if params.get('total_credit_liability',''):
            test.total_credit_liability = params.get('total_credit_liability','')
        if params.get('small_loan_total_loan_amount',''):
            test.small_loan_total_loan_amount = params.get('small_loan_total_loan_amount','')
        if params.get('microfinance_institutions_number',''):
            test.microfinance_institutions_number = params.get('microfinance_institutions_number','')
        if params.get('credit_report_number',''):
            test.credit_report_number = params.get('credit_report_number','')
        if params.get('is_microfinance_credit',''):
            test.is_microfinance_credit = params.get('is_microfinance_credit','')
        if params.get('is_microfinance_company_issues_loans',''):
            test.is_microfinance_company_issues_loans = params.get('is_microfinance_company_issues_loans','')
        if params.get('consumer_finance_company_total_loan_amount',''):
            test.consumer_finance_company_total_loan_amount = params.get('consumer_finance_company_total_loan_amount','')
        if params.get('consumer_finance_lending_institution_number',''):
            test.consumer_finance_lending_institution_number = params.get('consumer_finance_lending_institution_number','')
        if params.get('consumer_finance_credit_report_number',''):
            test.consumer_finance_credit_report_number = params.get('consumer_finance_credit_report_number','')
        if params.get('is_consumer_finance',''):
            test.is_consumer_finance = params.get('is_consumer_finance','')
        if params.get('is_consumer_finance_company_issue_loans',''):
            test.is_consumer_finance_company_issue_loans = params.get('is_consumer_finance_company_issue_loans','')
        if params.get('is_credit_liability',''):
            test.is_credit_liability = params.get('is_credit_liability','')
        if params.get('debt_amount',''):
            test.debt_amount = params.get('debt_amount','')
        if params.get('is_other_liability',''):
            test.is_other_liability = params.get('is_other_liability','')
        if params.get('is_loan',''):
            test.is_loan = params.get('is_loan','')
        if params.get('is_freeze',''):
            test.is_freeze = params.get('is_freeze','')
        if params.get('credit_a_month_number',''):
            test.credit_a_month_number = params.get('credit_a_month_number','')
        if params.get('credit_two_month_number',''):
            test.credit_two_month_number = params.get('credit_two_month_number','')
        if params.get('credit_three_month_number',''):
            test.credit_three_month_number = params.get('credit_three_month_number','')
        if params.get('credit_six_month_number',''):
            test.credit_six_month_number = params.get('credit_six_month_number','')
        if params.get('is_overdue_now',''):
            test.is_overdue_now = params.get('is_overdue_now','')
        if params.get('overdue_amount_now',''):
            test.overdue_amount_now = params.get('overdue_amount_now','')
        if params.get('is_overdue_half',''):
            test.is_overdue_half = params.get('is_overdue_half','')
        if params.get('overdue_amount_half',''):
            test.overdue_amount_half = params.get('overdue_amount_half','')
        if params.get('overdue_time_half',''):
            test.overdue_time_half = params.get('overdue_time_half','')
        if params.get('is_overdue_one',''):
            test.is_overdue_one = params.get('is_overdue_one','')
        if params.get('overdue_amount_one',''):
            test.overdue_amount_one = params.get('overdue_amount_one','')
        if params.get('overdue_time_one',''):
            test.overdue_time_one = params.get('overdue_time_one','')
        if params.get('is_overdue_two',''):
            test.is_overdue_two = params.get('is_overdue_two','')
        if params.get('overdue_amount_two',''):
            test.overdue_amount_two = params.get('overdue_amount_two','')
        if params.get('overdue_time_two',''):
            test.overdue_time_two = params.get('overdue_time_two','')
        if params.get('is_business_icense',''):
            test.is_business_icense = params.get('is_business_icense','')
        if params.get('is_business_icense_me',''):
            test.is_business_icense_me = params.get('is_business_icense_me','')
        if params.get('is_outside',''):
            test.is_outside = params.get('is_outside','')
        if params.get('is_work_place',''):
            test.is_work_place = params.get('is_work_place','')
        if params.get('business_license_registration_time',''):
            test.business_license_registration_time = params.get('business_license_registration_time','')
        if params.get('annual_tax_amount',''):
            test.annual_tax_amount = params.get('annual_tax_amount','')
        if params.get('is_normal_taxation',''):
            test.is_normal_taxation = params.get('is_normal_taxation','')
        if params.get('company_annual_income',''):
            test.company_annual_income = params.get('company_annual_income','')
        if params.get('occupation',''):
            test.occupation = params.get('occupation','')
        if params.get('monthly_bank_flow',''):
            test.monthly_bank_flow = params.get('monthly_bank_flow','')
        if params.get('is_bank_flow',''):
            test.is_bank_flow = params.get('is_bank_flow','')
        if params.get('unit_nature',''):
            test.unit_nature = params.get('unit_nature','')
        if params.get('salary_payment_method',''):
            test.salary_payment_method = params.get('salary_payment_method','')
        if params.get('salary',''):
            test.salary = params.get('salary','')
        if params.get('social_security_base',''):
            test.social_security_base = params.get('social_security_base','')
        if params.get('social_security_continuous_payment_time_social',''):
            test.social_security_continuous_payment_time_social = params.get('social_security_continuous_payment_time_social','')
        if params.get('social_security_payment_time',''):
            test.social_security_payment_time = params.get('social_security_payment_time','')
        if params.get('is_social_security_paid',''):
            test.is_social_security_paid = params.get('is_social_security_paid','')
        if params.get('social_security_payment_method',''):
            test.social_security_payment_method = params.get('social_security_payment_method','')
        if params.get('social_security_information',''):
            test.social_security_information = params.get('social_security_information','')
        if params.get('provident_fund_base',''):
            test.provident_fund_base = params.get('provident_fund_base','')
        if params.get('provident_fund_continuous_payment_time_provident',''):
            test.provident_fund_continuous_payment_time_provident = params.get('provident_fund_continuous_payment_time_provident','')
        if params.get('provident_fund_withdrawal_time',''):
            test.provident_fund_withdrawal_time = params.get('provident_fund_withdrawal_time','')
        if params.get('is_provident_fund_paid',''):
            test.is_provident_fund_paid = params.get('is_provident_fund_paid','')
        if params.get('provident_fund_payment_method',''):
            test.provident_fund_payment_method = params.get('provident_fund_payment_method','')
        if params.get('provident_fund_information',''):
            test.provident_fund_information = params.get('provident_fund_information','')
        if params.get('sesame_credit_score',''):
            test.sesame_credit_score = params.get('sesame_credit_score','')
        if params.get('is_alipay_sesame_credits',''):
            test.is_alipay_sesame_credits = params.get('is_alipay_sesame_credits','')
        if params.get('particle_loan_quota',''):
            test.particle_loan_quota = params.get('particle_loan_quota','')
        if params.get('is_tencent_micro_loan',''):
            test.is_tencent_micro_loan = params.get('is_tencent_micro_loan','')
        if params.get('white_line_quota',''):
            test.white_line_quota = params.get('white_line_quota','')
        if params.get('is_jindong_white_strip',''):
            test.is_jindong_white_strip = params.get('is_jindong_white_strip','')
        if params.get('borrowing_quota',''):
            test.borrowing_quota = params.get('borrowing_quota','')
        if params.get('is_ant_borrowing',''):
            test.is_ant_borrowing = params.get('is_ant_borrowing','')
        if params.get('recruitment_quota',''):
            test.recruitment_quota = params.get('recruitment_quota','')
        if params.get('is_recruitment_quota',''):
            test.is_recruitment_quota = params.get('is_recruitment_quota','')
        if params.get('gitzo_quota',''):
            test.gitzo_quota = params.get('gitzo_quota','')
        if params.get('is_gitzo_quota',''):
            test.is_gitzo_quota = params.get('is_gitzo_quota','')
        test.save()

        response = {
            'code': 200, 'ret': {'message': 'success'},
            "data":{
                "test_id":test.test_id
            }
        }
    except Exception, e:
        response['code'] = 404
        response['ret'] = {'message': str(e)}
    return JsonResponse(response)






@csrf_exempt
def pullTest(request):
    '''
        拉取评测信息
        POST
        传参：{  （除token均为可选参数）
            "token": ***
            "filter": "test_id,*,*..." //筛选返回数据,以逗号隔开，如果不填则返回所有数据
            "test_id": *** //若填写则精确返回该评测，若不填，则返回和该用户相关所有评测
        }

        返回：{
            "code": *
            "ret":{
                "massage": ***
            }
            "data":{
                //评测
                "test":[
                    {
                        "test_id": ***
                        ...所有信息(不包括创建时间和更新时间)...
                    },
                    ... ...
                ],
            }
        }
    '''
    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'POST':
        return JsonResponse(response)
    try:
        params = json.loads(request.body)

        token = params.get('token','')
        test_id = params.get('test_id','')

        if not parserToken(token):
            response = {'code': 401, 'ret': {'message': 'token err'}}
            return JsonResponse(response)

        info = token2dic(token)
        user_id = info["user_id"]

        all_list = []
        if test_id == '':
            ad = Test.objects.values_list('test_id',flat=True)
            for i in ad:
                if Test.objects.get(test_id=i).user_id.user_id == user_id:
                    all_list.append(i)
        else:
            if not Test.objects.get(test_id=int(test_id)).user_id.user_id == user_id:
                response = {'code': 403, 'ret': {'message': 'you have no power'}}
                return JsonResponse(response)
            all_list.append(int(test_id))

        def pull(id, Filter=[]):
            info = Test.objects.get(test_id=id)
            data = {}
            def p(a, b):
                if not Filter or b in Filter:
                    data[b] = a

            p(info.test_id, 'test_id')
            p(info.user_id.user_id, 'user_id')
            p(info.test_tag, 'test_tag')
            p(info.is_property, 'is_property')
            p(info.property_status, 'property_status')
            p(info.full_house, 'full_house')
            p(info.mortgage_house, 'mortgage_house')
            p(info.pledge_house, 'pledge_house')
            p(info.property_number, 'property_number')
            p(info.relevant_document_status, 'relevant_document_status')
            p(info.full_house_area, 'full_house_area')
            p(info.mortgage_house_area, 'mortgage_house_area')
            p(info.pleage_house_area, 'pleage_house_area')
            p(info.full_property_category, 'full_property_category')
            p(info.mortgage_property_category, 'mortgage_property_category')
            p(info.pledge_property_category, 'pledge_property_category')
            p(info.full_real_estate_area, 'full_real_estate_area')
            p(info.mortgage_real_estate_area, 'mortgage_real_estate_area')
            p(info.pledge_real_estate_area, 'pledge_real_estate_area')
            p(info.full_property_year, 'full_property_year')
            p(info.mortgage_property_year, 'mortgage_property_year')
            p(info.pledge_property_year, 'pledge_property_year')
            p(info.full_property_valuation, 'full_property_valuation')
            p(info.mortgage_property_valuation, 'mortgage_property_valuation')
            p(info.pledge_property_valuation, 'pledge_property_valuation')
            p(info.mortgage_monthly_time_house, 'mortgage_monthly_time_house')
            p(info.loan_amount, 'loan_amount')
            p(info.mortgage_monthly_payment_method, 'mortgage_monthly_payment_method')
            p(info.mortgage_monthly_repayment_amount, 'mortgage_monthly_repayment_amount')
            p(info.pledge_monthly_repayment_amount, 'pledge_monthly_repayment_amount')
            p(info.pledge_time, 'pledge_time')
            p(info.pledge_agency_house, 'pledge_agency_house')
            p(info.pledge_repayment_method_house, 'pledge_repayment_method_house')
            p(info.pledge_amount_house, 'pledge_amount_house')
            p(info.is_car, 'is_car')
            p(info.car_status, 'car_status')
            p(info.full_car, 'full_car')
            p(info.pledge_car, 'pledge_car')
            p(info.mortgage_car, 'mortgage_car')
            p(info.is_full_license_plate_attribution, 'is_full_license_plate_attribution')
            p(info.is_mortgage_license_plate_attribution, 'is_mortgage_license_plate_attribution')
            p(info.is_pledge_license_plate_attribution, 'is_pledge_license_plate_attribution')
            p(info.full_vehicle_mileage, 'full_vehicle_mileage')
            p(info.mortgage_vehicle_mileage, 'mortgage_vehicle_mileage')
            p(info.pledge_vehicle_mileage, 'pledge_vehicle_mileage')
            p(info.full_vehicle_bare_car_price, 'full_vehicle_bare_car_price')
            p(info.mortgage_vehicle_bare_car_price, 'mortgage_vehicle_bare_car_price')
            p(info.pledge_vehicle_bare_car_price, 'pledge_vehicle_bare_car_price')
            p(info.full_vehicle_life, 'full_vehicle_life')
            p(info.mortgage_vehicle_life, 'mortgage_vehicle_life')
            p(info.pledge_vehicle_life, 'pledge_vehicle_life')
            p(info.full_vehicle_transfer_time, 'full_vehicle_transfer_time')
            p(info.mortgage_vehicle_transfer_time, 'mortgage_vehicle_transfer_time')
            p(info.pledge_vehicle_transfer_time, 'pledge_vehicle_transfer_time')
            p(info.is_full_transfer, 'is_full_transfer')
            p(info.is_mortgage_transfer, 'is_mortgage_transfer')
            p(info.is_pledge_transfer, 'is_pledge_transfer')
            p(info.full_car_condition, 'full_car_condition')
            p(info.mortgage_car_condition, 'mortgage_car_condition')
            p(info.pledge_car_condition, 'pledge_car_condition')
            p(info.is_full_name, 'is_full_name')
            p(info.is_mortgage_name, 'is_mortgage_name')
            p(info.is_pledge_name, 'is_pledge_name')
            p(info.pledge_amount_car, 'pledge_amount_car')
            p(info.pledge_agency_car, 'pledge_agency_car')
            p(info.pledge_monthly_time_car, 'pledge_monthly_time_car')
            p(info.vehicle_loan_amount, 'vehicle_loan_amount')
            p(info.mortgage_monthly_time_car, 'mortgage_monthly_time_car')
            p(info.mortgage_agency, 'mortgage_agency')
            p(info.mortgage_repayment_method, 'mortgage_repayment_method')
            p(info.pledge_repayment_method_car, 'pledge_repayment_method_car')
            p(info.policy_annual_payment_amount, 'policy_annual_payment_amount')
            p(info.insurance_company, 'insurance_company')
            p(info.payment_method, 'payment_method')
            p(info.is_insured, 'is_insured')
            p(info.is_replace_insured, 'is_replace_insured')
            p(info.is_commercial_insurance_break, 'is_commercial_insurance_break')
            p(info.warranty, 'warranty')
            p(info.is_buy_commercial_insurance, 'is_buy_commercial_insurance')
            p(info.borrower_age, 'borrower_age')
            p(info.is_domicile, 'is_domicile')
            p(info.local_residence_time, 'local_residence_time')
            p(info.is_divorce_kid, 'is_divorce_kid')
            p(info.is_married_kid, 'is_married_kid')
            p(info.is_unmarried_kid, 'is_unmarried_kid')
            p(info.marital_status, 'marital_status')
            p(info.total_loan_amount, 'testtotal_loan_amount_tag')
            p(info.monthly_debt, 'monthly_debt')
            p(info.total_debt_amount, 'total_debt_amount')
            p(info.is_mortgage_liability, 'is_mortgage_liability')
            p(info.total_credit_cards, 'total_credit_cards')
            p(info.credit_card_total, 'credit_card_total')
            p(info.amount_used, 'amount_used')
            p(info.is_credit_card_liability, 'is_credit_card_liability')
            p(info.total_credit_liability, 'total_credit_liability')
            p(info.small_loan_total_loan_amount, 'small_loan_total_loan_amount')
            p(info.microfinance_institutions_number, 'microfinance_institutions_number')
            p(info.credit_report_number, 'credit_report_number')
            p(info.is_microfinance_credit, 'is_microfinance_credit')
            p(info.is_microfinance_company_issues_loans, 'is_microfinance_company_issues_loans')
            p(info.consumer_finance_company_total_loan_amount, 'consumer_finance_company_total_loan_amount')
            p(info.consumer_finance_lending_institution_number, 'consumer_finance_lending_institution_number')
            p(info.consumer_finance_credit_report_number, 'consumer_finance_credit_report_number')
            p(info.is_consumer_finance, 'is_consumer_finance')
            p(info.is_consumer_finance_company_issue_loans, 'is_consumer_finance_company_issue_loans')
            p(info.is_credit_liability, 'is_credit_liability')
            p(info.debt_amount, 'debt_amount')
            p(info.is_other_liability, 'is_other_liability')
            p(info.is_loan, 'is_loan')
            p(info.is_freeze, 'is_freeze')
            p(info.credit_a_month_number, 'credit_a_month_number')
            p(info.credit_two_month_number, 'credit_two_month_number')
            p(info.credit_three_month_number, 'credit_three_month_number')
            p(info.credit_six_month_number, 'credit_six_month_number')
            p(info.is_overdue_now, 'is_overdue_now')
            p(info.overdue_amount_now, 'overdue_amount_now')
            p(info.is_overdue_half, 'is_overdue_half')
            p(info.overdue_amount_half, 'overdue_amount_half')
            p(info.overdue_time_half, 'overdue_time_half')
            p(info.is_overdue_one, 'is_overdue_one')
            p(info.overdue_amount_one, 'overdue_amount_one')
            p(info.overdue_time_one, 'overdue_time_one')
            p(info.is_overdue_two, 'is_overdue_two')
            p(info.overdue_amount_two, 'overdue_amount_two')
            p(info.overdue_time_two, 'overdue_time_two')
            p(info.is_business_icense, 'is_business_icense')
            p(info.is_business_icense_me, 'is_business_icense_me')
            p(info.is_outside, 'is_outside')
            p(info.is_work_place, 'is_work_place')
            p(info.business_license_registration_time, 'business_license_registration_time')
            p(info.annual_tax_amount, 'annual_tax_amount')
            p(info.is_normal_taxation, 'is_normal_taxation')
            p(info.company_annual_income, 'company_annual_income')
            p(info.occupation, 'occupation')
            p(info.monthly_bank_flow, 'monthly_bank_flow')
            p(info.is_bank_flow, 'is_bank_flow')
            p(info.unit_nature, 'unit_nature')
            p(info.salary_payment_method, 'salary_payment_method')
            p(info.salary, 'salary')
            p(info.social_security_base, 'social_security_base')
            p(info.social_security_continuous_payment_time_social, 'social_security_continuous_payment_time_social')
            p(info.social_security_payment_time, 'social_security_payment_time')
            p(info.is_social_security_paid, 'is_social_security_paid')
            p(info.social_security_payment_method, 'social_security_payment_method')
            p(info.social_security_information, 'social_security_information')
            p(info.provident_fund_base, 'provident_fund_base')
            p(info.provident_fund_continuous_payment_time_provident, 'provident_fund_continuous_payment_time_provident')
            p(info.provident_fund_withdrawal_time, 'provident_fund_withdrawal_time')
            p(info.is_provident_fund_paid, 'is_provident_fund_paid')
            p(info.provident_fund_payment_method, 'provident_fund_payment_method')
            p(info.provident_fund_information, 'provident_fund_information')
            p(info.sesame_credit_score, 'sesame_credit_score')
            p(info.is_alipay_sesame_credits, 'is_alipay_sesame_credits')
            p(info.particle_loan_quota, 'particle_loan_quota')
            p(info.is_tencent_micro_loan, 'is_tencent_micro_loan')
            p(info.white_line_quota, 'white_line_quota')
            p(info.is_jindong_white_strip, 'is_jindong_white_strip')
            p(info.borrowing_quota, 'borrowing_quota')
            p(info.is_ant_borrowing, 'is_ant_borrowing')
            p(info.recruitment_quota, 'recruitment_quota')
            p(info.is_recruitment_quota, 'is_recruitment_quota')
            p(info.gitzo_quota, 'gitzo_quota')
            p(info.is_gitzo_quota, 'is_gitzo_quota')
            return data
        
        response = {
            'code': 200,
            'ret': {'message': 'success'},
            "data":{}
        }
        adFilter = params.get('filter','').replace(' ','')
        if adFilter == '':
            adFilter=[]
        else:
            adFilter = adFilter.split(',')
        result = []
        for data in all_list:
            result.append(pull(data, adFilter))
        response["data"]["test"] = result
    except Exception, e:
        response = {'code': 404, 'ret': {'message': str(e)}}
    return JsonResponse(response)