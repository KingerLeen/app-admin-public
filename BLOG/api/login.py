#!/usr/bin/env python
# encoding: utf-8

import datetime
import json
import base64
import jwt
import time
import uuid
from django import forms

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from Crypto.Cipher import AES

from ..models import *
from .. import views

from func import *

@csrf_exempt
def createToken(request):
    '''
    生成token
        GET

        传参：{
            "phone_number": ***
            "password": ***
        }

        返回：{
            "code": *
            "ret":{
                "message": ***
            }
            "data":{
                "token": ***
            }
        }

        备注:{  (token内容)
            签发者
            签发时间
            过期时间（10分钟）
            user_id
            phone_number
            password
        }
    '''
    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'GET':
        return JsonResponse(response)
    try:
        params =  request.GET
        phone_number = params.get('phone_number', '')
        password = params.get('password', '')

        if not checkPhone(phone_number):
            response = {'code': 400, 'ret': {'message': u'电话号码错误或未注册'}}
            return JsonResponse(response)

        if not checkPassword(phone_number, password):
            response = {'code': 400, 'ret': {'message': u'密码错误'}}
            return JsonResponse(response)

        info = {
            'iss': 'app',
            'iat': time.time(),
            'exp': time.time() + 600,
            'user_id': User.objects.get(phone_number=phone_number).user_id,
            'phone_number': phone_number,
            'password': password
        }
        result = jwt.encode(info, SECRET, algorithm='HS256')
        response = {
            'code': 200,
            'ret': {'message': 'success'},
            "data": {'token': result}
        }
    except Exception, e:
        response['code'] = 404
        response['ret'] = {'message': str(e)}
    return JsonResponse(response)



@csrf_exempt
def checkToken(request):
    '''
    验证token
        GET

        传参{
            "token": ***
        }

        返回{
            "code": *
            "ret":{
                "message": ***
            }
        }
    '''
    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'GET':
        return JsonResponse(response)
    try:
        params =  request.GET
        token = params.get('token','')

        if(parserToken(token)):
            response = {'code': 200, 'ret': {'message': 'pass'}}
            return JsonResponse(response)
        else:
            response = {'code': 401, 'ret': {'message': 'token err'}}
            return JsonResponse(response)
    except Exception, e:
        response['code'] = 404
        response['ret'] = {'message': str(e)}
    return JsonResponse(response)




@csrf_exempt
def login(request):
    ''' 
    登录验证
        GET

        传参：{
            "phone_number": ***
            "password": ***
        }

        返回：{
            "code": *
            "ret":{
                "massage": ***
            }
        }
    '''
    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'GET':
        return JsonResponse(response)
    try:
        params =  request.GET

        phone_number = params.get('phone_number', '')
        password = params.get('password', '')

        if not checkPhone(phone_number):
            response = {'code': 400, 'ret': {'message': u'电话号码错误或未注册'}}
            return JsonResponse(response)

        if not checkPassword(phone_number,password):
            response = {'code': 400, 'ret': {'message': u'密码错误'}}
            return JsonResponse(response)

        info = {
            'iss': 'app',
            'iat': time.time(),
            'exp': time.time() + 600,
            'user_id': User.objects.get(phone_number=phone_number).user_id,
            'phone_number': phone_number,
            'password': password
        }
        result = jwt.encode(info, SECRET, algorithm='HS256')
        response = {
            'code': 200,
            'ret': {'message': 'success'},
            "data": {'token': result}
        }
    except Exception, e:
        response['code'] = 404
        response['ret'] = {'message': str(e)}
    return JsonResponse(response)