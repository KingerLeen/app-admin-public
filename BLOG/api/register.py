#!/usr/bin/env python
# encoding: utf-8

import datetime
import json
import base64
import jwt
import time
import uuid
from django import forms

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from Crypto.Cipher import AES

from ..models import *
from .. import views

from func import *




@csrf_exempt
def register(request):
    ''' 
    注册
        GET

        传参：{
            "name": ***姓名
            "phone_number": ***电话号码
            "verification_code": ***验证码
            "password": ***密码
            "invited_code": ***(可选)邀请码
        }

        返回：{
            "code": *
            "ret":{
                "massage": ***
            }
        }
    '''

    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'POST':
        return JsonResponse(response)
    try:
        params = json.loads(request.body)
        name = params.get('name','')
        phone_number = params.get('phone_number','')
        verification_code = params.get('verification_code','')
        password = params.get('password','')
        invited_code = params.get('invited_code','')
        
        if not phone_number or checkPhone(phone_number):
            response = {'code': 400, 'ret': {'message': u'该电话号码已被注册'}}
            return JsonResponse(response)
        
        if invited_code:
            if not User.objects.filter(invite_code=invited_code):
                response = {'code': 400, 'ret': {'message': u'没有找到该邀请码'}}
                return JsonResponse(response)

        User.objects.create(
            phone_number=phone_number,
            name=name,
            password=password,
            is_manager=False,
            invite_code=str(uuid.uuid1()).replace('-',''),
            invited_code=invited_code
        )
        response = {'code': 200, 'ret': {'message': 'success'}}
    except Exception, e:
        response['code'] = 404
        response['ret'] = {'message': str(e)}
    return JsonResponse(response)

