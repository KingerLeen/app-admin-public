#!/usr/bin/env python
# encoding: utf-8

import datetime
import json
import base64
import jwt
import time
import uuid
from django import forms

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from Crypto.Cipher import AES

from ..models import *
from .. import views

from func import checkUser
from func import checkUserPassword
from func import *


@csrf_exempt
def pullOrder(request):
    '''
        拉取Order信息
        GET
        传参：{（除token 其余均为可选参数）
            "token": ***
            "filter": "order_id,order_state,*,*" //筛选返回数据,以逗号隔开，如果不填则默认返回所有数据
            "order_id": *** //若填写则精确返回该订单，若不填，则返回 和该用户相关的(订单经理和用户) 所有订单
        }

        返回：{
            "code": *
            "ret":{
                "massage": ***
            }
            "data":{
                "order_123" (123为order_id) :{
                    "order_id": ***
                    ...所有信息(不包括创建时间和更新时间)...
                    图片返回的是文件地址，例如：media/contents/a.jpeg 
                    加上服务器地址即可直接访问该图片 ip:8000/media/contents/a.jpeg
                },
                //如果订单不止一个，按照上面的格式，一一返回
                    ... ...
            }
        }
    '''

    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'GET':
        return JsonResponse(response)
    try:
        params =  request.GET
        token = params.get('token','')
        if not parserToken(token):
            response = {'code': 401, 'ret': {'message': 'token error'}}
            return JsonResponse(response)
        info = token2dic(token)

        order_id = params.get('order_id','')

        all_list = []
        if order_id == '':
            user = Order.objects.filter(user_id=info["user_id"]).values_list('order_id',flat=True)
            all_list += user

            product_id_list=Product.objects.filter(product_manager=info["user_id"]).values_list('product_id',flat=True)
            for p in product_id_list:
                manager = Order.objects.filter(product_id=p).values_list('order_id',flat=True)
                all_list += manager
        else:
            sql = Order.objects.get(order_id=order_id)
            if sql.user_id.user_id == info["user_id"] or sql.product_id.product_manager == info["user_id"]:
                all_list.append(order_id)
            else:
                response = {'code': 403, 'ret': {'message': 'no order'}}
                return JsonResponse(response)
        all_list = list(set(all_list))

        def pullOrderInfo(order_id, Filter=[]):
            listOrder = Order.objects.get(order_id=order_id)
            data = {}
            if not Filter or 'order_id' in Filter:
                data["order_id"]=listOrder.order_id
            if not Filter or 'order_state' in Filter:
                data["order_state"]=listOrder.order_state
            if not Filter or 'product_id' in Filter:
                data["product_id"]=listOrder.product_id.product_id
            if not Filter or 'user_id' in Filter:
                data["user_id"]=listOrder.user_id.user_id
            if not Filter or 'ioan_amount' in Filter:
                data["ioan_amount"]=listOrder.ioan_amount
            if not Filter or 'expect_earnings' in Filter:
                data["expect_earnings"]=listOrder.expect_earnings
            return data
        
        response = {
            'code': 200,
            'ret': {'message': 'success'},
            "data":{}
        }

        orderFilter = params.get('filter','').replace(' ','')
        if orderFilter == '':
            orderFilter=[]
        else:
            orderFilter = orderFilter.split(',')
        
        print JsonResponse(pullOrderInfo(18))

        for data in all_list:
            print data
            response["data"]["order_"+str(data)] = pullOrderInfo(data, orderFilter)
    except Exception, e:
        response = {'code': 404, 'ret': {'message': str(e)}}
    return JsonResponse(response)

@csrf_exempt
def createOrder(request):
    ''' 
    创建新的订单
        POST

        传参：
        body:  (JSON)  (均为必填参数)
        {
            "token": ***
            "order_state": ***
            "product_id": ***
            "ioan_amount": ***
            "ioan_term": ***
            "expect_earnings": ***
        }

        返回：{
            "code": *
            "ret":{
                "massage": ***
            }
        }
    '''
    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'POST':
        return JsonResponse(response)
    try:
        payload = json.loads(request.body)
        token = payload['token']
        
        if not parserToken(token):
            response = {'code': 401, 'ret': {'message': 'token error'}}
            return JsonResponse(response)
        
        info = token2dic(token)
        Order.objects.create(
            order_state = payload['order_state'],
            product_id = Product.objects.get(product_id=payload['product_id']),
            user_id = User.objects.get(user_id=info['user_id']),
            ioan_amount = payload['ioan_amount'],
            ioan_term = payload['ioan_term'],
            expect_earnings = payload['expect_earnings']
        )

        response = {'code': 200, 'ret': {'message': 'success'}}
    except Exception, e:
        response['code'] = 404
        response['ret'] = {'message': str(e)}
    return JsonResponse(response)

@csrf_exempt
def changeOrder(request):
    ''' 
    修改订单信息
        POST

        传参：
        body:  (JSON)  (除 token和order_id 其余均为可选参数,即要修改的内容)
        {
            "token":
            "order_id":
            "order_state":
            "ioan_amount":
            "ioan_term":
            "expect_earnings":
        }

        返回：{
            "code": *
            "ret":{
                "massage": ***/success
            }
        }
    '''
    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'POST':
        return JsonResponse(response)
    try:
        payload = json.loads(request.body)
        token = payload['token']
        
        if not parserToken(token):
            response = {'code': 401, 'ret': {'message': 'token error'}}
            return JsonResponse(response)
        
        info = token2dic(token)
        order = Order.objects.get(order_id=payload['order_id'])
        if order.user_id != User.objects.get(user_id=info['user_id']):
            response = {'code': 403, 'ret': {'message': 'no order'}}
            return JsonResponse(response)
        
        response = {'code': 200, 'ret': {'message': 'success'}}
        changeOrderInfo(order, payload, response)
    except Exception, e:
        response = {'code': 404, 'ret': {'message': 'request error'}}
        response['ret'] = {'message': str(e)}
    return JsonResponse(response)
