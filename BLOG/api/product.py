#!/usr/bin/env python
# encoding: utf-8

import datetime
import json
import base64
import jwt
import time
import uuid
from django import forms

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from Crypto.Cipher import AES

from ..models import *
from .. import views

from func import *

@csrf_exempt
def pullProductRecommend(request):
    '''
        GET    拉取产品库信息

        传参：{
            "type": 0=本月推荐,车贷信息 1=本月推荐,房贷信息 2=本月推荐,信贷信息
        }

        返回：{
            "code": *
            "ret":{
                "massage": ***
            }
            "data":{
                //本月推荐 信息
                "recommend":[
                    {
                        "product_id": ***
                        ...所有信息(不包括创建时间和更新时间)...
                    },
                    ... ...
                ],
                //图片返回的是文件地址，例如：media/contents/a.jpeg
                //加上服务器地址和端口即可直接访问该图片 ip:8000/media/contents/a.jpeg
            }
        }
    '''
    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'GET':
        return JsonResponse(response)
    try:
        params =  request.GET
        typeP = params.get('type','')

        response = {
            'code': 200,
            'ret': {'message': 'success'},
            "data":{}
        }

        def pullProduct(product_id):
            l = Product.objects.get(product_id=product_id)
            data = {
                "product_id": l.product_id,
                "product_manager": l.product_manager.user_id,
                "product_name": l.product_name,
                "product_icon": str(l.product_icon),
                "product_bank": l.product_bank.bank_id,
                "tag": l.tag,
                "ioan_type": l.ioan_type,
                "application": l.application,
                "limit_scope": l.limit_scope,
                "term_scope": l.term_scope,
                "monthly_interest_rate_scope": l.monthly_interest_rate_scope,
                "poundage": l.poundage,
                "service_cost": l.service_cost,
                "required_information": l.required_information,
                "recommend": l.recommend
            }
            return data

        def pullProduct_only(product_id):
            l = Product.objects.get(product_id=product_id)
            data = {
                "product_id": l.product_id,
                "product_name": l.product_name,
                "product_bank": l.product_bank,
            }
            return data
        
        recommendList = []
        
        product = Product.objects.values_list('product_id',flat=True)
        for i in product:
            result = pullProduct(i)
            if result["recommend"]==True:
                if typeP=="0":
                    if result["ioan_type"]==u"车贷" :
                        recommendList.append(result)
                elif typeP=="1":
                    if result["ioan_type"]==u"房贷" :
                        recommendList.append(result)
                elif typeP=="2":
                    if result["ioan_type"]==u"信贷" :
                        recommendList.append(result)

        response["data"]["recommend"] = recommendList

    except Exception, e:
        response = {'code': 404, 'ret': {'message': str(e)}}
    return JsonResponse(response)










@csrf_exempt
def pullProductBank(request):
    '''
        GET    拉取产品库信息

        传参：{
            "type": 0=车贷相关银行 1=房贷相关银行 2=信贷相关银行
        }

        返回：{
            "code": *
            "ret":{
                "massage": ***
            }
            "data":{
                //银行
                "bank":[
                    {
                        "bank_id": ***
                        "bank_name": ***
                        "bank_logo": ***
                    },
                    ... ...
                ]

                //图片返回的是文件地址，例如：media/contents/a.jpeg
                //加上服务器地址和端口即可直接访问该图片 ip:8000/media/contents/a.jpeg
            }
        }
    '''
    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'GET':
        return JsonResponse(response)
    try:
        params =  request.GET
        typeP = params.get('type','')

        response = {
            'code': 200,
            'ret': {'message': 'success'},
            "data":{}
        }
        
        thisBankId = []
        t = u" "
        if typeP == "0":
            t = u"车贷"
        elif typeP == "1":
            t = u"房贷"
        elif typeP == "2":
            t = u"信贷"
        for i in Product.objects.values_list('product_id',flat=True):
            p = Product.objects.get(product_id=i)
            if p.ioan_type == t and (not (p.product_bank.bank_id in thisBankId)):
                thisBankId.append(p.product_bank.bank_id)
        
        bankList = []
        bank = Bank.objects.values_list('bank_id',flat=True)
        for i in bank:
            l = Bank.objects.get(bank_id=i)
            if l.bank_id in thisBankId:
                data = {
                    "bank_id": l.bank_id,
                    "bank_name": l.bank_name,
                    "bank_icon": str(l.bank_icon)
                }
                bankList.append(data)

        response["data"]["bank"] = bankList

    except Exception, e:
        response = {'code': 404, 'ret': {'message': str(e)}}
    return JsonResponse(response)








@csrf_exempt
def pullProduct(request):
    '''
        GET    拉取产品详细信息

        传参：{
            "product_id": ***
            "filter": "order_id,order_state,*,*" //筛选返回数据,以逗号隔开，如果不填则默认返回所有数据
        }

        返回：{
            "code": *
            "ret":{
                "massage": ***
            }
            "data":{
                "product_id": ***
                "product_name": ***
                ...所有信息(不包括创建时间和更新时间)...

                //图片返回的是文件地址，例如：media/contents/a.jpeg
                //加上服务器地址和端口即可直接访问该图片 ip:8000/media/contents/a.jpeg
            }
        }
    '''
    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'GET':
        return JsonResponse(response)
    try:
        params =  request.GET
        product_id = params.get('product_id','')

        productFilter = params.get('filter','').replace(' ','')
        if productFilter == '':
            productFilter=[]
        else:
            productFilter = productFilter.split(',')

        response = {
            'code': 200,
            'ret': {'message': 'success'},
            "data":{}
        }

        def pullProduct(product_id, productFilter):
            l = Product.objects.get(product_id=product_id)
            data = {}
            if not productFilter or 'product_id' in productFilter:
                data["product_id"] = l.product_id
            if not productFilter or 'product_manager' in productFilter:
                data["product_manager"] = l.product_manager.user_id
            if not productFilter or 'product_name' in productFilter:
                data["product_name"] = l.product_name
            if not productFilter or 'product_icon' in productFilter:
                data["product_icon"] = str(l.product_icon)
            if not productFilter or 'product_bank' in productFilter:
                data["product_bank"] = l.product_bank
            if not productFilter or 'tag' in productFilter:
                data["tag"] = l.tag
            if not productFilter or 'ioan_type' in productFilter:
                data["ioan_type"] = l.ioan_type
            if not productFilter or 'application' in productFilter:
                data["application"] = l.application
            if not productFilter or 'limit_scope' in productFilter:
                data["limit_scope"] = l.limit_scope
            if not productFilter or 'term_scope' in productFilter:
                data["term_scope"] = l.term_scope
            if not productFilter or 'monthly_interest_rate_scope' in productFilter:
                data["monthly_interest_rate_scope"] = l.monthly_interest_rate_scope
            if not productFilter or 'poundage' in productFilter:
                data["poundage"] = l.poundage
            if not productFilter or 'service_cost' in productFilter:
                data["service_cost"] = l.service_cost
            if not productFilter or 'required_information' in productFilter:
                data["required_information"] = l.required_information
            if not productFilter or 'recommend' in productFilter:
                data["recommend"] = l.recommend
            return data
        
        response["data"] = pullProduct(product_id,productFilter)

    except Exception, e:
        response = {'code': 404, 'ret': {'message': str(e)}}
    return JsonResponse(response)





































# @csrf_exempt
# def createProduct(request):
#     ''' 
#     创建新的产品
#         POST

#         传参：
#         body:  (JSON)  (均为必填参数)
#         {
#             "token": ***
#             "product_name": ***
#             "interest": ***
#             "tag": ***
#             "limit_scope": ***
#             "term_scope": ***
#             "repayment_type": ***
#             "poundage": ***
#             "order_process": ***
#             "application": ***
#             "application_information": ***
#             "receiver_name": ***
#             "receiver_position": ***
#             "receiver_grade": ***
#             "receiver_contact_way"
#             "loan_time"
#             "penalty"
#             "flow"
#             "letter_credit_condition"
#             "is_letter_credit"
#             "region"
#             "is_psopopc"
#             "special"
#             "is_white"
#             "product_organization"
#             "loan_average_limit"
#             "recent_pass_rate"
#             "is_contact"
#             "is_contact_know"
#             "is_work_proof"
#             "is_personal_account"
#             "is_visit"
#             "total_interest"
#         }

#         返回：{
#             "code": *
#             "ret":{
#                 "massage": ***
#             }
#         }
#     '''
#     response = {'code': 405, 'ret': {'message': 'request error'}}
#     if request.method != 'POST':
#         return JsonResponse(response)
#     try:
#         payload = json.loads(request.body)
#         token = payload['token']
        
#         if not parserToken(token):
#             response = {'code': 401, 'ret': {'message': 'token error'}}
#             return JsonResponse(response)
        
#         info = token2dic(token)
#         user = User.objects.get(user_id=info['user_id'])
#         if not user.is_manager:
#             response = {'code': 403, 'ret': {'message': 'you are not manager'}}
#             return JsonResponse(response)

#         Product.objects.create(
#             product_manager = user,
#             product_name = payload['product_name'],
#             interest = payload['interest'],
#             tag = payload['tag'],
#             limit_scope = payload['limit_scope'],
#             term_scope = payload['term_scope'],
#             repayment_type = payload['repayment_type'],
#             poundage = payload['poundage'],
#             order_process = payload['order_process'],
#             application = payload['application'],
#             application_information = payload['application_information'],
#             receiver_name = payload['receiver_name'],
#             receiver_position = payload['receiver_position'],
#             receiver_grade = payload['receiver_grade'],
#             receiver_contact_way = payload['receiver_contact_way'],
#             loan_time = payload['loan_time'],
#             penalty = payload['penalty'],
#             flow = payload['flow'],
#             letter_credit_condition = payload['letter_credit_condition'],
#             is_letter_credit = payload['is_letter_credit'],
#             region = payload['region'],
#             is_psopopc = payload['is_psopopc'],
#             special = payload['special'],
#             is_white = payload['is_white'],
#             product_organization = payload['product_organization'],
#             loan_average_limit = payload['loan_average_limit'],
#             recent_pass_rate = payload['recent_pass_rate'],
#             is_contact = payload['is_contact'],
#             is_contact_know = payload['is_contact_know'],
#             is_work_proof = payload['is_work_proof'],
#             is_personal_account = payload['is_personal_account'],
#             is_visit = payload['is_visit'],
#             total_interest = payload['total_interest'],
#         )
#         response = {'code': 200, 'ret': {'message': 'success'}}
#     except Exception, e:
#         response['code'] = 404
#         response['ret'] = {'message': str(e)}
#     return JsonResponse(response)

# @csrf_exempt
# def changeProduct(request):
#     ''' 
#     修改产品信息
#         POST

#         传参：
#         body:  (JSON)  (除 token和product_id 其余均为可选参数,即要修改的内容)
#         {
#             "token": ***
#             "product_id": ***
#             "product_name": ***
#             "interest": ***
#             "tag": ***
#             "limit_scope": ***
#             "term_scope": ***
#             "repayment_type": ***
#             "poundage": ***
#             "order_process": ***
#             "application": ***
#             "application_information": ***
#             "receiver_name": ***
#             "receiver_position": ***
#             "receiver_grade": ***
#             "receiver_contact_way"
#             "loan_time"
#             "penalty"
#             "flow"
#             "letter_credit_condition"
#             "is_letter_credit"
#             "region"
#             "is_psopopc"
#             "special"
#             "is_white"
#             "product_organization"
#             "loan_average_limit"
#             "recent_pass_rate"
#             "is_contact"
#             "is_contact_know"
#             "is_work_proof"
#             "is_personal_account"
#             "is_visit"
#             "total_interest"
#         }

#         返回：{
#             "code": *
#             "ret":{
#                 "massage": ***/success
#             }
#         }
#     '''
#     response = {'code': 405, 'ret': {'message': 'request error'}}
#     if request.method != 'POST':
#         return JsonResponse(response)
#     try:
#         payload = json.loads(request.body)
#         token = payload['token']
        
#         if not parserToken(token):
#             response = {'code': 401, 'ret': {'message': 'token error'}}
#             return JsonResponse(response)
        
#         info = token2dic(token)
#         product=Product.objects.get(product_id=payload['product_id'])
        
#         if product.product_manager != User.objects.get(user_id=info['user_id']):
#             response = {'code': 403, 'ret': {'message': 'you have no power'}}
#             return JsonResponse(response)
        
#         response = {'code': 200, 'ret': {'message': 'success'}}
#         changeProductInfo(product, payload, response)
#     except Exception, e:
#         response = {'code': 404, 'ret': {'message': 'request error'}}
#         response['ret'] = {'message': str(e)}
#     return JsonResponse(response)

# @csrf_exempt
# def pullProduct(request):
#     '''
#         拉取Product信息
#         GET
#         传参：{（均为可选参数）
#             "filter": "order_id,order_state,*,*" //筛选返回数据,以逗号隔开，如果不填则默认返回所有数据
#             "product_id": *** //若填写则精确返回该订单，若不填，则返回 和该用户相关的(订单经理和用户) 所有订单
#         }

#         返回：{
#             "code": *
#             "ret":{
#                 "massage": ***
#             }
#             "data":{
#                 "product_123" (123为product_id) :{
#                     "product_id": ***
#                     ...所有信息(不包括创建时间和更新时间)...
#                     图片返回的是文件地址，例如：media/contents/a.jpeg 
#                     加上服务器地址即可直接访问该图片 ip:8000/media/contents/a.jpeg
#                 },
#                 //如果订单不止一个，按照上面的格式，一一返回
#                     ... ...
#             }
#         }
#     '''

#     response = {'code': 405, 'ret': {'message': 'request error'}}
#     if request.method != 'GET':
#         return JsonResponse(response)
#     try:
#         params =  request.GET

#         product_id = params.get('product_id','')

#         all_list = []
#         if product_id == '':
#             product = Product.objects.values_list('product_id',flat=True)
#             all_list += product
#         else:
#             all_list.append(Product.objects.get(product_id=product_id).product_id)

#         def pullProductInfo(product_id, Filter=[]):
#             listProduct = Product.objects.get(product_id=product_id)
#             data = {}
#             if not Filter or 'product_id' in Filter:
#                 data["product_id"]=listProduct.product_id
#             if not Filter or 'product_manager' in Filter:
#                 data["product_manager"]=listProduct.product_manager.user_id
#             if not Filter or 'product_name' in Filter:
#                 data["product_name"]=listProduct.product_name
#             if not Filter or 'product_icon' in Filter:
#                 data["product_icon"]=str(listProduct.product_icon)
#             if not Filter or 'interest' in Filter:
#                 data["interest"]=str(listProduct.interest) 
#             if not Filter or 'tag' in Filter:
#                 data["tag"]=listProduct.tag
#             if not Filter or 'limit_scope' in Filter:
#                 data["limit_scope"]=listProduct.limit_scope
#             if not Filter or 'repayment_type' in Filter:
#                 data["repayment_type"]=listProduct.repayment_type
#             if not Filter or 'poundage' in Filter:
#                 data["poundage"]=listProduct.poundage
#             if not Filter or 'order_process' in Filter:
#                 data["order_process"]=listProduct.order_process
#             if not Filter or 'application' in Filter:
#                 data["application"]=listProduct.application
#             if not Filter or 'application_information' in Filter:
#                 data["application_information"]=listProduct.application_information
#             if not Filter or 'receiver_name' in Filter:
#                 data["receiver_name"]=listProduct.receiver_name
#             if not Filter or 'receiver_position' in Filter:
#                 data["receiver_position"]=listProduct.receiver_position
#             if not Filter or 'receiver_grade' in Filter:
#                 data["receiver_grade"]=listProduct.receiver_grade
#             if not Filter or 'receiver_contact_way' in Filter:
#                 data["receiver_contact_way"]=listProduct.receiver_contact_way
#             if not Filter or 'loan_time' in Filter:
#                 data["loan_time"]=listProduct.loan_time
#             if not Filter or 'penalty' in Filter:
#                 data["penalty"]=listProduct.penalty
#             if not Filter or 'flow' in Filter:
#                 data["flow"]=listProduct.flow
#             if not Filter or 'letter_credit_condition' in Filter:
#                 data["letter_credit_condition"]=listProduct.letter_credit_condition
#             if not Filter or 'is_letter_credit' in Filter:
#                 data["is_letter_credit"]=listProduct.is_letter_credit
#             if not Filter or 'region' in Filter:
#                 data["region"]=listProduct.region
#             if not Filter or 'is_psopopc' in Filter:
#                 data["is_psopopc"]=listProduct.is_psopopc
#             if not Filter or 'special' in Filter:
#                 data["special"]=listProduct.special
#             if not Filter or 'is_white' in Filter:
#                 data["is_white"]=listProduct.is_white
#             if not Filter or 'product_organization' in Filter:
#                 data["product_organization"]=listProduct.product_organization
#             if not Filter or 'loan_average_limit' in Filter:
#                 data["loan_average_limit"]=listProduct.loan_average_limit
#             if not Filter or 'recent_pass_rate' in Filter:
#                 data["recent_pass_rate"]=listProduct.recent_pass_rate
#             if not Filter or 'is_contact' in Filter:
#                 data["is_contact"]=listProduct.is_contact
#             if not Filter or 'is_contact_know' in Filter:
#                 data["is_contact_know"]=listProduct.is_contact_know
#             if not Filter or 'is_work_proof' in Filter:
#                 data["is_work_proof"]=listProduct.is_work_proof
#             if not Filter or 'is_personal_account' in Filter:
#                 data["is_personal_account"]=listProduct.is_personal_account
#             if not Filter or 'is_visit' in Filter:
#                 data["is_visit"]=listProduct.is_visit
#             if not Filter or 'total_interest' in Filter:
#                 data["total_interest"]=listProduct.total_interest
#             return data
#         response = {
#             'code': 200,
#             'ret': {'message': 'success'},
#             "data":{}
#         }

#         productFilter = params.get('filter','').replace(' ','')
#         if productFilter == '':
#             productFilter=[]
#         else:
#             productFilter = productFilter.split(',')

#         for data in all_list:
#             response["data"]["product_"+str(data)] = pullProductInfo(data, productFilter)
#     except Exception, e:
#         response = {'code': 404, 'ret': {'message': str(e)}}
#     return JsonResponse(response)

# @csrf_exempt
# def uploadProductIron(request):
#     ''' 
#     上传产品图片
#         POST

#         传参：
#         body:  (form-data))  (token 和 product_id 为必填参数)
#         {
#             "token":
#             "product_id":
#             "product_icon":
#         }

#         返回：{
#             "code": *
#             "ret":{
#                 "massage": ***
#             }
#         }
#     '''
#     response = {'code': 405, 'ret': {'message': 'request error'}}
#     if request.method != 'POST':
#         return JsonResponse(response)
#     try:
#         p = request.POST
#         token = p.get('token')

#         if not parserToken(token):
#             response = {'code': 401, 'ret': {'message': 'token error'}}
#             return JsonResponse(response)

#         info = token2dic(token)
#         product = Product.objects.get(product_id=p.get('product_id'))

#         if not product.product_manager.user_id == info["user_id"]:
#             response = {'code': 404, 'ret': {'message': 'you have no power'}}
#             return JsonResponse(response)

#         product_icon = request.FILES.get('product_icon')
#         if product_icon:
#             product.product_icon=product_icon
#         product.save()

#         response = {'code': 200, 'ret': {'message': 'success'}}
#     except Exception, e:
#         response['ret'] = {'message': str(e)}
#     return JsonResponse(response)







