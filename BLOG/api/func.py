#!/usr/bin/env python
# encoding: utf-8

import datetime
import json
import base64
import jwt
import time
import uuid
from django import forms

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from Crypto.Cipher import AES

from ..models import *
from .. import views

SECRET = 'bloc'

def checkPhone(phone_number):
    '''
        验证电话号码是否存在
    '''
    try:
        if User.objects.filter(phone_number=phone_number):
            return True
        return False
    except Exception:
        return False

def checkTest(test_id):
    '''
        验证test_id是否存在
    '''
    try:
        if Test.objects.filter(test_id=test_id):
            return True
        return False
    except Exception:
        return False

def checkPassword(phone_number, password):
    '''
        验证用户名密码是否正确
    '''
    try:
        if User.objects.filter(phone_number=phone_number, password=password):
            return True
        return False
    except Exception:
        return False

def token2dic(token):
    '''
        token转换成字典
    '''
    try:
        info = jwt.decode(token, SECRET, algorithms=['HS256'])
    except Exception:
        return {}
    return info

def parserToken(token):
    '''
        验证token是否通过
    '''
    try:
        info = jwt.decode(token, SECRET, algorithms=['HS256'])
    except Exception:
        return False

    if checkPassword(info['phone_number'],info['password']) and info['exp'] > time.time():
        return True
    return False

def hasField(payload, field):
    try:
        payload[field]
    except Exception:
        return False
    return True

def changeUserInfo(db, payload, response):
    '''
        "token":
        "user_name":
        "password":
        "collect":
        "sex":
        "age":
        "region":
        "household_register":
        "marital_status":
        "monthly_salary":
        "fixed_assets":
        "accumulation_fund":
        "name":
        "mail":
        "phone_number":
        "career_type":
        "customer_qualification":
        "id_card":
        "is_letter_credit":
        "remarks":
        "work_phone":
        "work_nature":
        "work_address":
        "work_name":
        "contact_name":
        "contact_number":
        "contact_relationship":
        "social_security":
    '''
    if hasField(payload, "user_name"):
        if (not checkUser(payload["user_name"])) and payload["user_name"]:
            db.user_name = payload["user_name"]
            response["ret"].update(user_name = "success")
        else:
            response['code'] = 400
            response["ret"].update(user_name = "user_name repeat")
            return

    if hasField(payload, "password"):
        db.password = payload["password"]
        response["ret"].update(password = "success")

    if hasField(payload, "collect"):
        db.collect = payload["collect"]
        response["ret"].update(collect = "success")
    
    db.save()
    db = Information.objects.get(user_id=db.user_id)
    changeInformationInfo(db, payload, response)

def changeInformationInfo(db, payload, response):
    if hasField(payload, "sex"):
        db.sex = payload["sex"]
        response["ret"].update(sex = "success")

    if hasField(payload, "age"):
        db.age = payload["age"]
        response["ret"].update(age = "success")

    if hasField(payload, "region"):
        db.region = payload["region"]
        response["ret"].update(region = "success")

    if hasField(payload, "household_register"):
        db.household_register = payload["household_register"]
        response["ret"].update(household_register = "success")

    if hasField(payload, "marital_status"):
        db.marital_status = payload["marital_status"]
        response["ret"].update(marital_status = "success")

    if hasField(payload, "monthly_salary"):
        db.monthly_salary = payload["monthly_salary"]
        response["ret"].update(monthly_salary = "success")

    if hasField(payload, "fixed_assets"):
        db.fixed_assets = payload["fixed_assets"]
        response["ret"].update(fixed_assets = "success")

    if hasField(payload, "accumulation_fund"):
        db.accumulation_fund = payload["accumulation_fund"]
        response["ret"].update(accumulation_fund = "success")

    if hasField(payload, "name"):
        db.name = payload["name"]
        response["ret"].update(name = "success")

    if hasField(payload, "mail"):
        db.mail = payload["mail"]
        response["ret"].update(mail = "success")

    if hasField(payload, "phone_number"):
        db.phone_number = payload["phone_number"]
        response["ret"].update(phone_number = "success")

    if hasField(payload, "career_type"):
        db.career_type = payload["career_type"]
        response["ret"].update(career_type = "success")

    if hasField(payload, "customer_qualification"):
        db.customer_qualification = payload["customer_qualification"]
        response["ret"].update(customer_qualification = "success")

    if hasField(payload, "id_card"):
        db.id_card = payload["id_card"]
        response["ret"].update(id_card = "success")

    if hasField(payload, "is_letter_credit"):
        db.is_letter_credit = payload["is_letter_credit"]
        response["ret"].update(is_letter_credit = "success")

    if hasField(payload, "remarks"):
        db.remarks = payload["remarks"]
        response["ret"].update(remarks = "success")

    if hasField(payload, "work_phone"):
        db.work_phone = payload["work_phone"]
        response["ret"].update(work_phone = "success")

    if hasField(payload, "work_nature"):
        db.work_nature = payload["work_nature"]
        response["ret"].update(work_nature = "success")

    if hasField(payload, "work_address"):
        db.work_address = payload["work_address"]
        response["ret"].update(work_address = "success")

    if hasField(payload, "work_name"):
        db.work_name = payload["work_name"]
        response["ret"].update(work_name = "success")

    if hasField(payload, "contact_name"):
        db.contact_name = payload["contact_name"]
        response["ret"].update(contact_name = "success")

    if hasField(payload, "contact_number"):
        db.contact_number = payload["contact_number"]
        response["ret"].update(contact_number = "success")

    if hasField(payload, "contact_relationship"):
        db.contact_relationship = payload["contact_relationship"]
        response["ret"].update(contact_relationship = "success")

    if hasField(payload, "social_security"):
        db.social_security = payload["social_security"]
        response["ret"].update(social_security = "success")

    db.save()

def changeOrderInfo(db, payload, response):
    if hasField(payload, "order_state"):
        db.order_state = payload["order_state"]
        response["ret"].update(order_state = "success")

    if hasField(payload, "ioan_amount"):
        db.ioan_amount = payload["ioan_amount"]
        response["ret"].update(ioan_amount = "success")

    if hasField(payload, "ioan_term"):
        db.ioan_term = payload["ioan_term"]
        response["ret"].update(ioan_term = "success")

    if hasField(payload, "expect_earnings"):
        db.expect_earnings = payload["expect_earnings"]
        response["ret"].update(expect_earnings = "success")

    db.save()

def changeProductInfo(db, payload, response):
    if hasField(payload, "product_name"):
        db.product_name = payload["product_name"]
        response["ret"].update(product_name = "success")
    
    if hasField(payload, "interest"):
        db.interest = payload["interest"]
        response["ret"].update(interest = "success")

    if hasField(payload, "tag"):
        db.tag = payload["tag"]
        response["ret"].update(tag = "success")

    if hasField(payload, "limit_scope"):
        db.limit_scope = payload["limit_scope"]
        response["ret"].update(limit_scope = "success")

    if hasField(payload, "term_scope"):
        db.term_scope = payload["term_scope"]
        response["ret"].update(term_scope = "success")

    if hasField(payload, "repayment_type"):
        db.repayment_type = payload["repayment_type"]
        response["ret"].update(repayment_type = "success")

    if hasField(payload, "poundage"):
        db.poundage = payload["poundage"]
        response["ret"].update(poundage = "success")

    if hasField(payload, "order_process"):
        db.order_process = payload["order_process"]
        response["ret"].update(order_process = "success")

    if hasField(payload, "application"):
        db.application = payload["application"]
        response["ret"].update(application = "success")
        
    if hasField(payload, "application_information"):
        db.application_information = payload["application_information"]
        response["ret"].update(application_information = "success")

    if hasField(payload, "receiver_name"):
        db.receiver_name = payload["receiver_name"]
        response["ret"].update(receiver_name = "success")

    if hasField(payload, "receiver_position"):
        db.receiver_position = payload["receiver_position"]
        response["ret"].update(receiver_position = "success")

    if hasField(payload, "receiver_grade"):
        db.receiver_grade = payload["receiver_grade"]
        response["ret"].update(receiver_grade = "success")

    if hasField(payload, "receiver_contact_way"):
            db.receiver_contact_way = payload["receiver_contact_way"]
            response["ret"].update(receiver_contact_way = "success")

    if hasField(payload, "loan_time"):
            db.loan_time = payload["loan_time"]
            response["ret"].update(loan_time = "success")

    if hasField(payload, "penalty"):
            db.penalty = payload["penalty"]
            response["ret"].update(penalty = "success")

    if hasField(payload, "flow"):
            db.flow = payload["flow"]
            response["ret"].update(flow = "success")

    if hasField(payload, "letter_credit_condition"):
            db.letter_credit_condition = payload["letter_credit_condition"]
            response["ret"].update(letter_credit_condition = "success")

    if hasField(payload, "is_letter_credit"):
            db.is_letter_credit = payload["is_letter_credit"]
            response["ret"].update(is_letter_credit = "success")

    if hasField(payload, "region"):
            db.region = payload["region"]
            response["ret"].update(region = "success")

    if hasField(payload, "is_psopopc"):
            db.is_psopopc = payload["is_psopopc"]
            response["ret"].update(is_psopopc = "success")

    if hasField(payload, "special"):
            db.special = payload["special"]
            response["ret"].update(special = "success")

    if hasField(payload, "is_white"):
            db.is_white = payload["is_white"]
            response["ret"].update(is_white = "success")

    if hasField(payload, "product_organization"):
            db.product_organization = payload["product_organization"]
            response["ret"].update(product_organization = "success")

    if hasField(payload, "loan_average_limit"):
            db.loan_average_limit = payload["loan_average_limit"]
            response["ret"].update(loan_average_limit = "success")

    if hasField(payload, "recent_pass_rate"):
            db.recent_pass_rate = payload["recent_pass_rate"]
            response["ret"].update(recent_pass_rate = "success")

    if hasField(payload, "is_contact"):
            db.is_contact = payload["is_contact"]
            response["ret"].update(is_contact = "success")

    if hasField(payload, "is_contact_know"):
            db.is_contact_know = payload["is_contact_know"]
            response["ret"].update(is_contact_know = "success")

    if hasField(payload, "is_work_proof"):
            db.is_work_proof = payload["is_work_proof"]
            response["ret"].update(is_work_proof = "success")

    if hasField(payload, "is_personal_account"):
            db.is_personal_account = payload["is_personal_account"]
            response["ret"].update(is_personal_account = "success")

    if hasField(payload, "is_visit"):
            db.is_visit = payload["is_visit"]
            response["ret"].update(is_visit = "success")

    if hasField(payload, "total_interest"):
            db.total_interest = payload["total_interest"]
            response["ret"].update(total_interest = "success")
        
    db.save()
