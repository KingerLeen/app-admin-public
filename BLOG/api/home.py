#!/usr/bin/env python
# encoding: utf-8

import datetime
import json
import base64
import jwt
import time
import uuid
from django import forms

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from Crypto.Cipher import AES

from ..models import *
from .. import views

from func import *


@csrf_exempt
def home(request):
    '''
        GET    拉取首页 轮播广告 和 首页推荐产品 信息

        传参：{
        }

        返回：{
            "code": *
            "ret":{
                "massage": ***
            }
            "data":{
                //轮播广告
                "ad":[
                    {
                        "ad_id": ***
                        ...所有信息(不包括创建时间和更新时间)...
                    },
                    ... ...
                ],
                //首页推荐产品
                "recommend":[
                    {
                        "product_id": ***
                        ...所有信息(不包括创建时间和更新时间)...
                    },
                    ... ...
                ],
                //本月上新
                "new":[
                    {
                        "product_id": ***
                        ...所有信息(不包括创建时间和更新时间)...
                    },
                    ... ...
                ],
                
                //图片返回的是文件地址，例如：media/contents/a.jpeg
                //加上服务器地址和端口即可直接访问该图片 ip:8000/media/contents/a.jpeg
            }
        }
    '''
    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'GET':
        return JsonResponse(response)
    try:
        response = {
            'code': 200,
            'ret': {'message': 'success'},
            "data":{}
        }

        ad = Advertisement.objects.values_list('ad_id',flat=True)
        product = Product.objects.values_list('product_id',flat=True)

        def pullAd(ad_id):
            l = Advertisement.objects.get(ad_id=ad_id)
            data = {
                "ad_id": l.ad_id,
                "ad_url": l.ad_url,
                "ad_photo": str(l.ad_photo),
                "is_play": l.is_play
            }
            return data

        def pullProduct(product_id,func):
            l = Product.objects.get(product_id=product_id)
            if func == 1:
                if not l.recommend:
                    return False
            if func == 2:
                now_time = datetime.now().strftime(r'%Y-%m-%d %H:%M:%S')
                this_time = l.create_time.strftime(r'%Y-%m-%d %H:%M:%S')

                now_day = datetime.strptime(now_time, r'%Y-%m-%d %H:%M:%S')
                this_day = datetime.strptime(this_time, r'%Y-%m-%d %H:%M:%S')

                if (now_day - this_day).days > 30:
                    return False
            data = {
                "product_id": l.product_id,
                "product_manager": l.product_manager.user_id,
                "product_name": l.product_name,
                "product_icon": str(l.product_icon),
                "tag": l.tag,
                "ioan_type": l.ioan_type,
                "application": l.application,
                "limit_scope": l.limit_scope,
                "term_scope": l.term_scope,
                "monthly_interest_rate_scope": l.monthly_interest_rate_scope,
                "poundage": l.poundage,
                "service_cost": l.service_cost,
                "required_information": l.required_information,
                "recommend": l.recommend
            }
            return data
        
        adList = []
        recommendList = []
        newList = []

        for i in ad:
            adList.append(pullAd(i))
        for i in product:
            result = pullProduct(i,1)
            if result:
                recommendList.append(result)
            result = pullProduct(i,2)
            if result:
                newList.append(result)

        response["data"]["ad"] = adList
        response["data"]["recommend"] = recommendList
        response["data"]["new"] = newList

    except Exception, e:
        response = {'code': 404, 'ret': {'message': str(e)}}
    return JsonResponse(response)