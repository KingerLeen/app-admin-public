#!/usr/bin/env python
# encoding: utf-8

import datetime
import json
import base64
import jwt
import time
import uuid
from django import forms

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from Crypto.Cipher import AES

from ..models import *
from .. import views

from func import checkUser
from func import checkUserPassword
from func import *


@csrf_exempt
def changeUser(request):
    ''' 
    修改用户信息
        POST

        传参：
        body:  (JSON)  (除token 其余均为可选参数)
        {
            "token":
            "user_name":
            "password":
            "collect":
            "sex":
            "age":
            "region":
            "household_register":
            "marital_status":
            "monthly_salary":
            "fixed_assets":
            "accumulation_fund":
            "name":
            "mail":
            "phone_number":
            "career_type":
            "customer_qualification":
            "id_card":
            "is_letter_credit":
            "remarks":
            "work_phone":
            "work_nature":
            "work_address":
            "work_name":
            "contact_name":
            "contact_number":
            "contact_relationship":
            "social_security":
        }

        返回：{
            "code": *
            "ret":{
                "massage": ***success
            }
        }
    '''
    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'POST':
        return JsonResponse(response)
    try:
        payload = json.loads(request.body)
        token = payload['token']
        
        if not parserToken(token):
            response = {'code': 401, 'ret': {'message': 'token error'}}
            return JsonResponse(response)
        
        info = token2dic(token)
        db = User.objects.get(user_id=info['user_id'])
        response = {'code': 200, 'ret': {'message': 'success'}}
        changeUserInfo(db, payload, response)
    except Exception, e:
        response = {'code': 404, 'ret': {'message': 'request error'}}
        response['ret'] = {'message': str(e)}
    return JsonResponse(response)



@csrf_exempt
def pullUser(request):
    '''
        拉取User信息
        GET
        传参：{
            "token": *** //如果token不通过，则返回user_id查询到的可公开信息:
                (user_id,user_name,icon,rank,is_manager)
            "user_id": //只在不通过token的时候启用
            "filter": "user_id,user_name,*,*" //筛选返回数据,以逗号隔开，如果不填则默认返回所有数据
        }

        返回：{
            "code": *
            "ret":{
                "massage": ***
            }
            "data":{
                "user_id": ***
                ...所有信息(不包括创建时间和更新时间)...
                图片返回的是文件地址，例如：media/contents/a.jpeg 
                加上服务器地址即可直接访问该图片 ip:8000/media/contents/a.jpeg
            }
        }
    '''

    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'GET':
        return JsonResponse(response)
    try:
        params =  request.GET
        token = params.get('token','')
        if not parserToken(token):
            user_id = params.get('user_id','')
            if not user_id:
                response = {'code': 401, 'ret': {'message': 'token error'}}
                return JsonResponse(response)
            else:
                user = User.objects.get(user_id=user_id)
                response = {
                    'code': 200,
                    'ret': {'message': 'success'},
                    "data":{
                        "user_id":user.user_id,
                        "user_name":user.user_name,
                        "icon":str(user.icon),
                        "rank":user.rank,
                        "is_manager":user.is_manager
                    }
                }
                return JsonResponse(response)
            
        info = token2dic(token)

        listUser = User.objects.get(user_id=info['user_id'])
        listInformation = Information.objects.get(user_id=listUser.user_id)
        
        
        response = {
            'code': 200,
            'ret': {'message': 'success'},
            "data":{}
        }

        userFilter = params.get('filter','').replace(' ','')
        if userFilter == '':
            userFilter=[]
        else:
            userFilter = userFilter.split(',')
            
        if not userFilter or 'user_id' in userFilter:
            response["data"]["user_id"]=listUser.user_id
        if not userFilter or 'user_name' in userFilter:
            response["data"]["user_name"]=listUser.user_name
        if not userFilter or 'password' in userFilter:
            response["data"]["password"]=listUser.password
        if not userFilter or 'icon' in userFilter:
            response["data"]["icon"]=str(listUser.icon)
        if not userFilter or 'rank' in userFilter:
            response["data"]["rank"]=listUser.rank
        if not userFilter or 'is_manager' in userFilter:
            response["data"]["is_manager"]=listUser.is_manager
        if not userFilter or 'invite_code' in userFilter:
            response["data"]["invite_code"]=listUser.invite_code
        if not userFilter or 'invited_code' in userFilter:
            response["data"]["invited_code"]=listUser.invited_code
        if not userFilter or 'point' in userFilter:
            response["data"]["point"]=listUser.point
        if not userFilter or 'collect' in userFilter:
            response["data"]["collect"]=listUser.collect
        if not userFilter or 'sex' in userFilter:
            response["data"]["sex"]=listInformation.sex
        if not userFilter or 'limit' in userFilter:
            response["data"]["limit"]=listInformation.limit
        if not userFilter or 'age' in userFilter:
            response["data"]["age"]=listInformation.age
        if not userFilter or 'region' in userFilter:
            response["data"]["region"]=listInformation.region
        if not userFilter or 'household_register' in userFilter:
            response["data"]["household_register"]=listInformation.household_register
        if not userFilter or 'marital_status' in userFilter:
            response["data"]["marital_status"]=listInformation.marital_status
        if not userFilter or 'monthly_salary' in userFilter:
            response["data"]["monthly_salary"]=listInformation.monthly_salary
        if not userFilter or 'fixed_assets' in userFilter:
            response["data"]["fixed_assets"]=listInformation.fixed_assets
        if not userFilter or 'accumulation_fund' in userFilter:
            response["data"]["accumulation_fund"]=listInformation.accumulation_fund
        if not userFilter or 'name' in userFilter:
            response["data"]["name"]=listInformation.name
        if not userFilter or 'mail' in userFilter:
            response["data"]["mail"]=listInformation.mail
        if not userFilter or 'phone_number' in userFilter:
            response["data"]["phone_number"]=listInformation.phone_number
        if not userFilter or 'career_type' in userFilter:
            response["data"]["career_type"]=listInformation.career_type
        if not userFilter or 'customer_qualification' in userFilter:
            response["data"]["customer_qualification"]=listInformation.customer_qualification
        if not userFilter or 'id_card' in userFilter:
            response["data"]["id_card"]=listInformation.id_card
        if not userFilter or 'id_card_scanner' in userFilter:
            response["data"]["id_card_scanner"]=str(listInformation.id_card_scanner)
        if not userFilter or 'id_card_copy' in userFilter:
            response["data"]["id_card_copy"]=str(listInformation.id_card_copy)
        if not userFilter or 'handheld_card_photo' in userFilter:
            response["data"]["handheld_card_photo"]=str(listInformation.handheld_card_photo)
        if not userFilter or 'is_letter_credit' in userFilter:
            response["data"]["is_letter_credit"]=listInformation.is_letter_credit
        if not userFilter or 'enclosure' in userFilter:
            response["data"]["enclosure"]=str(listInformation.enclosure)
        if not userFilter or 'remarks' in userFilter:
            response["data"]["remarks"]=listInformation.remarks
        if not userFilter or 'work_phone' in userFilter:
            response["data"]["work_phone"]=listInformation.work_phone
        if not userFilter or 'work_nature' in userFilter:
            response["data"]["work_nature"]=listInformation.work_nature
        if not userFilter or 'work_address' in userFilter:
            response["data"]["work_address"]=listInformation.work_address
        if not userFilter or 'work_name' in userFilter:
            response["data"]["work_name"]=listInformation.work_name
        if not userFilter or 'contact_name' in userFilter:
            response["data"]["contact_name"]=listInformation.contact_name
        if not userFilter or 'contact_number' in userFilter:
            response["data"]["contact_number"]=listInformation.contact_number
        if not userFilter or 'contact_relationship' in userFilter:
            response["data"]["contact_relationship"]=listInformation.contact_relationship
        if not userFilter or 'social_security' in userFilter:
            response["data"]["social_security"]=listInformation.social_security
        
    except Exception, e:
        response = {'code': 404, 'ret': {'message': str(e)}}
    return JsonResponse(response)

@csrf_exempt
def uploadUserImg(request):
    ''' 
    上传用户头像等信息
        POST

        传参：
        body:  (form-data))  (token为必填参数，其余为选填)
        {
            "token":
            "icon":
            "id_card_scanner":
            "id_card_copy":
            "handheld_card_photo":
            "enclosure":
        }

        返回：{
            "code": *
            "ret":{
                "massage": ***
            }
        }
    '''
    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'POST':
        return JsonResponse(response)
    try:
        token = request.POST.get('token')

        if not parserToken(token):
            response = {'code': 401, 'ret': {'message': 'token error'}}
            return JsonResponse(response)

        info = token2dic(token)
        user = User.objects.get(user_id=info["user_id"])

        icon = request.FILES.get('icon')
        if icon:
            user.icon=icon

        user = Information.objects.get(user_id=user)

        id_card_scanner = request.FILES.get('id_card_scanner')
        if id_card_scanner:
            user.id_card_scanner=id_card_scanner
        
        id_card_copy = request.FILES.get('id_card_copy')
        if id_card_copy:
            user.id_card_copy=id_card_copy

        handheld_card_photo = request.FILES.get('handheld_card_photo')
        if handheld_card_photo:
            user.handheld_card_photo=handheld_card_photo

        enclosure = request.FILES.get('enclosure')
        if enclosure:
            user.enclosure=enclosure
        
        user.save()
                
        response = {'code': 200, 'ret': {'message': 'success'}}
    except Exception, e:
        response['code'] = 404
        response['ret'] = {'message': str(e)}
    return JsonResponse(response)
