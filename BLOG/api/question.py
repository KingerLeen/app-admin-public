#!/usr/bin/env python
# encoding: utf-8

import datetime
import json
import base64
import jwt
import time
import uuid
from django import forms

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from Crypto.Cipher import AES

from ..models import *
from .. import views
from func import *

@csrf_exempt
def question(request):
    ''' 
    常见问题提问
        GET

        传参：{
            "token": ***
            "product_id":
            "question_content"
        }

        返回：{
            "code": *
            "ret":{
                "massage": ***
            }
        }
    '''

    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'GET':
        return JsonResponse(response)
    try:
        params =  request.GET
        token = params.get('token','')
        product_id = params.get('product_id','')
        question_content = params.get('question_content','')

        if not parserToken(token):
            response = {'code': 401, 'ret': {'message': 'token error'}}
            return JsonResponse(response)

        info = token2dic(token)
        user = User.objects.get(user_id=info['user_id'])
        Question.objects.create(
            user_id=user,
            product_id=Product.objects.get(product_id=product_id),
            question_content=question_content,
            answer_content="",
            is_show=False
        )

        response = {'code': 200, 'ret': {'message': 'success'}}
    except Exception, e:
        response['code'] = 404
        response['ret'] = {'message': str(e)}
    return JsonResponse(response)