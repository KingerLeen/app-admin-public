# coding: utf-8
from xadmin.views import CommAdminView, ModelFormAdminView, ListAdminView, CreateAdminView, UpdateAdminView
from xadmin.layout import Main, Fieldset
from BLOG.models import *
import xadmin
from BLOG.plugins import ueditor
from django.utils.translation import ugettext as _
from django.utils.encoding import force_unicode
from django.conf import settings
import requests
import datetime

from exponent import send_push_message

class AdvertisementAdmin(object):
    list_display = ('ad_id', 'ad_url', 'ad_photo', 'is_play', 'create_time', 'update_time')

class UserAdmin(object):
    list_display = ('phone_number', 'rank', 'is_manager', 'point', 'create_time', 'update_time')

class BankAdmin(object):
    list_display = ('bank_id', 'bank_name', 'create_time', 'update_time')

class ProductAdmin(object):
    list_display = ('product_id', 'product_manager', 'product_name', 'create_time', 'update_time')

class OrderAdmin(object):
    list_display = ('order_id', 'product_id', 'user_id', 'ioan_amount', 'ioan_term', 'create_time', 'update_time')

class AdviseAdmin(object):
    list_display = ('advise_id', 'user_id', 'feedback_content', 'create_time', 'update_time')

class RuleAdmin(object):
    list_display = ('rule_id', 'evaluation_results', 'sex', 'limit', 'age', 'create_time', 'update_time')

xadmin.site.register(Advertisement, AdvertisementAdmin)
xadmin.site.register(User, UserAdmin)
xadmin.site.register(Bank, BankAdmin)
xadmin.site.register(Product, ProductAdmin)
xadmin.site.register(Order, OrderAdmin)
xadmin.site.register(Advise, AdviseAdmin)
xadmin.site.register(Rule, RuleAdmin)

class BookstoreAdmin(object):
    list_display = ('book_id', 'book_name', 'book_number', 'book_state', 'book_tag', 'create_time', 'update_time')
class TestAdmin(object):
    list_display = ('test_id', 'user_id', 'test_tag', 'create_time', 'update_time')
xadmin.site.register(Test, TestAdmin)
xadmin.site.register(Bookstore, BookstoreAdmin)

class QuestionAdmin(object):
    list_display = ('question_id', 'question_content', 'answer_content', 'is_show')
xadmin.site.register(Question, QuestionAdmin)

