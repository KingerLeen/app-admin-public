### 本地安装
* 基础构建

```
cd app-admin
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt -i https://pypi.douban.com/simple
```

* 初始化数据库

```
cd app-admin
python manage.py makemigrations
python manage.py migrate
```

* 收集静态文件

```
python manage.py collectstatic
```

* 初始化超级用户

```
python manage.py createsuperuser
```
* 项目启动

```
cd app-admin
virtualenv venv
python manage.py runserver
```
