"""ksCMS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
import xadmin
from ksCMS import settings
from django.contrib.admin import TabularInline
from django.conf.urls.static import static
from BLOG import views

xadmin.autodiscover()
handler404 = views.page_not_found
handler500 = views.page_not_found

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^news$', views.index, name='index'),
    url(r'^detail$', views.index, name='index'),
    url(r'^join$', views.index, name='index'),
    url(r'^admin/', include(xadmin.site.urls)),
    url(r'^ueditor/', include('DjangoUeditor.urls')),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^apis/', include('BLOG.urls')),
]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
